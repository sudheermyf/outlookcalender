﻿using OutlookCalendar.Model;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Media.Animation;
using Office365Communication;
using Office365Communication.Meetings;
using OutlookCalendar.UserControls.AlertMessageControls;
using OutlookCalendar.UserControls.ScheduleControls; 
using System.Configuration;
using System.Reflection;
using System.ComponentModel;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for Schedule.xaml
    /// </summary>
    public partial class Schedule : UserControl
    {
        public Schedule()
        {
            InitializeComponent();
        }
        ExchangeService service;
        Process proc;
        private string processName, Message;
        public DateTime SelectedDate { get; set; }
        Configuration config;
        private List<string> lstEmailAddress;
        public event EventHandler EvntSchErrorMsg;
        public event EventHandler EvntSchSuccessMsg;
        public event EventHandler EvntScheduleToCalendar;
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            btnSave.IsEnabled = true;
            LogHelper.Logger.Info("Starts UserControl_Loaded_1 Function in UserControls/Schedule.cs");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            lstEmailAddress = new List<string>();
            txtLocation.Text = Convert.ToString(config.AppSettings.Settings["RoomSettings"].Value);
            txtEvent.Focus();
            if (SelectedDate < DateTime.Now)
            {
                timePckr.Value = DateTime.Now;
            }
            else
            {
                timePckr.Value = SelectedDate;
            }
            
            service = Apps.service;

            BackgroundWorker bgSchedulerLoad = new BackgroundWorker();
            bgSchedulerLoad.WorkerReportsProgress = true;
            bgSchedulerLoad.WorkerSupportsCancellation = true;
            bgSchedulerLoad.DoWork += bgSchedulerLoad_DoWork;
            bgSchedulerLoad.RunWorkerCompleted += bgSchedulerLoad_RunWorkerCompleted;
            if (bgSchedulerLoad != null && !bgSchedulerLoad.IsBusy)
            {
                bgSchedulerLoad.RunWorkerAsync();
            }
            LogHelper.Logger.Info("Ends UserControl_Loaded_1 Function in UserControls/Schedule.cs");
        }

        void bgSchedulerLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RemoveLoadingScreen();
        }

        void bgSchedulerLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    Apps.PingTest();
                    Loading loading = new Loading();
                    Panel.SetZIndex(loading, 9999);
                    loading.BringIntoView();
                    loading.txtBlckLoading.Text = "Loading Scheduler... Please wait...";
                    ScheduleLayout.Children.Add(loading);
                }));

                NameResolutionCollection collection = Office365Communication.Contacts.O365Contacts.GetSavedContacts(service, "s");
                foreach (NameResolution item in collection)
                {
                    lstEmailAddress.Add(item.Mailbox.Address);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception while fetch Contacts.", ex);
            }
        }
        private void RemoveLoadingScreen()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in ScheduleLayout.Children)
            {
                if (item is Loading)
                {
                    lstIndexes.Add(ScheduleLayout.Children.IndexOf(item));
                }
            }

            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                ScheduleLayout.Children.RemoveAt(itemIndex);
            }
        }

        Response resp = null;
        Appointment appointment = null;
        private void btnSave_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                Apps.PingTest();
                if (cmbMinutes.SelectedIndex.Equals(0))
                {
                    Message = "Meeting time should be greater than 0.";
                    MessageBox.Show(Message, "DRL", MessageBoxButton.OK, MessageBoxImage.Error);
                    //EvntSchErrorMsg(Message, null);     //event to display error alert message
                }

                if (!string.IsNullOrEmpty(txtEvent.Text) && !string.IsNullOrEmpty(txtAttendees.Text.Trim()))
                {
                    btnSave.IsEnabled = false;
                    LogHelper.Logger.Info("Starts btnSave_PreviewTouchUp_1 Function in UserControls/Schedule.cs");
                    this.Cursor = Cursors.Wait;
                    DateTime dtStartDate = DateTime.Now;
                    DateTime dtEndDate = DateTime.Now;

                    string startDate = datePckr.Value.Value.ToShortDateString() + "  " + timePckr.Value.Value.Hour + ":" + timePckr.Value.Value.Minute;
                    dtStartDate = Convert.ToDateTime(startDate);

                    dtEndDate = dtStartDate.AddMinutes(GetSelectedMinutes());

                    if (dtStartDate < dtEndDate && txtEvent.Text != null)
                    {
                        service = Apps.service;
                        appointment = new Appointment(service);
                        appointment.Subject = txtEvent.Text;
                        appointment.Body = txtMessage.Text;
                        appointment.Location = txtLocation.Text;
                        appointment.Start = dtStartDate;
                        appointment.End = dtEndDate;
                        appointment.IsReminderSet = true;
                        appointment.ReminderMinutesBeforeStart = 15;
                        string[] lstAttendee = txtAttendees.Text.Split(',');
                        foreach (var item in lstAttendee)
                        {
                            if (!string.IsNullOrWhiteSpace(item))
                                appointment.RequiredAttendees.Add(item);
                        }
                        appointment.RequiredAttendees.Add(txtLocation.Text);

                        BackgroundWorker bgSaveScheduler = new BackgroundWorker();
                        bgSaveScheduler.WorkerReportsProgress = true;
                        bgSaveScheduler.WorkerSupportsCancellation = true;
                        bgSaveScheduler.DoWork += bgSaveScheduler_DoWork;
                        bgSaveScheduler.RunWorkerCompleted += bgSaveScheduler_RunWorkerCompleted;
                        if (bgSaveScheduler != null && !bgSaveScheduler.IsBusy)
                        {
                            bgSaveScheduler.RunWorkerAsync();
                        }
                    }
                }
                else
                {
                    Message = "Required fields are empty.";
                    MessageBox.Show(Message, "DRL", MessageBoxButton.OK, MessageBoxImage.Error);
                    //EvntSchErrorMsg(Message, null);     //event to display error alert message
                }
                LogHelper.Logger.Info("Ends btnSave_PreviewTouchUp_1 Function in UserControls/Schedule.cs");
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Arrow;
                LogHelper.Logger.ErrorException("Exception from btnSave.", ex);
            }
            finally
            {
                btnSave.IsEnabled = true;
            }
        }

        private double GetSelectedMinutes()
        {
            switch (cmbMinutes.SelectedIndex)
            {
                case 0:
                    return 0;
                case 1:
                default:
                    return 30;
                case 2:
                    return 60;
                case 3:
                    return 120;
            }
        }

        void bgSaveScheduler_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (resp != null && resp.IsSuccess)
                {
                    Message = "Appointment added successfully !!!";
                    ClearInputFields();
                    System.Threading.Thread.Sleep(6000);
                    //EvntSchSuccessMsg(Message, null);   //event to display success alert message
                    btnBack_PreviewTouchUp_1(sender, null);
                }
                else
                {
                    Message = "Appointment adding failed !!!";
                    //EvntSchErrorMsg(Message, null);     //event to display error alert message
                    txtEvent.Focus();
                }
                RemoveLoadingScreen();
                //CloseOSK(); //close virtual keyboard if open
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception while Saving schedule on Worker Completed. Error: " + ex.Message, ex);
            }
        }

        void bgSaveScheduler_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                    {
                        Apps.PingTest();
                        Loading loading = new Loading();
                        Panel.SetZIndex(loading, 9999);
                        loading.BringIntoView();
                        loading.txtBlckLoading.Text = "Creating Appointment... Please wait...";
                        ScheduleLayout.Children.Add(loading);
                    }));

                Appointments appointments = new Appointments();
                resp = appointments.CreateAppoinment(appointment, service);
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception while Saving schedule on Do Worker. Error: " + ex.Message, ex);
            }
        }
        private void ClearInputFields()
        {
            LogHelper.Logger.Info("Starts ClearInputFields Function in UserControls/Schedule.cs");
            txtAttendees.Text = string.Empty;
            //txtBody.Text = string.Empty;
            txtEvent.Text = string.Empty;
            //tblkRoomName.Text = string.Empty;
            ////cmbAvlMeetingRoom.SelectedIndex = -1;
            //timePckr.Value = DateTime.Now;
            ////timePckrEnd.Value = DateTime.Now;
            LogHelper.Logger.Info("Ends ClearInputFields Function in UserControls/Schedule.cs");
        }
        //private void OpenVirtualKeyboard()
        //{
        //    try
        //    {
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //        ////EvntSchErrorMsg(ex.Message, null);
        //    }
        //}
        //private void CloseOSK()
        //{
        //    LogHelper.Logger.Info("Starts CloseOSK Function in UserControls/Schedule.cs");
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //    LogHelper.Logger.Info("Ends CloseOSK Function in UserControls/Schedule.cs");
        //}
        
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnBack_PreviewTouchUp_1 Function in UserControls/Schedule.cs");
            Apps.KillOSK();
            EvntScheduleToCalendar(this, null);
            LogHelper.Logger.Info("Ends btnBack_PreviewTouchUp_1 Function in UserControls/Schedule.cs");
        }
        private void txtBody_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtBody_TouchDown Function in UserControls/Schedule.cs");
            Apps.OpenVirtualKeyboard();  //open virtual keyboard
            //txtBody.Focus();
            LogHelper.Logger.Info("Ends txtBody_TouchDown Function in UserControls/Schedule.cs");
        }
        private void txtEvent_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtEvent_TouchDown Function in UserControls/Schedule.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtEvent_TouchDown Function in UserControls/Schedule.cs");
        }
        private void txtEvent_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtEvent_TouchLeave Function in UserControls/Schedule.cs");
            Apps.OpenVirtualKeyboard();
            txtEvent.Focus();
            LogHelper.Logger.Info("Ends txtEvent_TouchLeave Function in UserControls/Schedule.cs");
        }
        private void btnAddAttendee_PreviewTouchUp(object sender, TouchEventArgs e)
        {
        }
        private void LoadAttendee()
        {
            LogHelper.Logger.Info("Starts LoadAttendee Function in UserControls/Schedule.cs");
            ContactsList cList = new ContactsList();
            ScheduleLayout.Children.Add(cList);
            //raise events from clist
            cList.EvntAddContactList -= cList_EvntAddContactList;
            cList.EvntCloseContactList -= cList_EvntCloseContactList;
            
            cList.EvntAddContactList += cList_EvntAddContactList;
            cList.EvntCloseContactList += cList_EvntCloseContactList;
            //EvntOpenContactList(this, null);
            LogHelper.Logger.Info("ends LoadAttendee Function in UserControls/Schedule.cs");
        }
        void cList_EvntCloseContactList(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts cList_EvntCloseContactList Function in UserControls/Schedule.cs");
            ContactsList cList = (ContactsList)sender;
            ScheduleLayout.Children.Remove(cList);
            LogHelper.Logger.Info("ends cList_EvntCloseContactList Function in UserControls/Schedule.cs");
        }
        void cList_EvntAddContactList(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts cList_EvntAddContactList Function in UserControls/Schedule.cs");
            string attendeeList = sender as string;
            if(!string.IsNullOrEmpty(attendeeList))
            {
                string[] lstAttendee = attendeeList.Split(',');
                foreach (var item in lstAttendee)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        if (txtAttendees.Text.IndexOf(item) == -1)
                        {
                            txtAttendees.Text = (txtAttendees.Text + "," + item).TrimStart(',');
                        }
                    }
                }
                CloseContactList();
            }
            else
            {
                Message = "Please Select atleast One Contact !!!";
                MessageBox.Show(Message, "DRL", MessageBoxButton.OK, MessageBoxImage.Error);
                //EvntSchErrorMsg(Message, null); 

            }
           
            LogHelper.Logger.Info("ends cList_EvntAddContactList Function in UserControls/Schedule.cs");
        }
        private void CloseContactList()
        {
            LogHelper.Logger.Info("Starts CloseContactList Function in UserControls/Schedule.cs");
            List<int> lstIndexes = new List<int>(); //list to store ContactsList index value in mainLayout
            foreach (UIElement item in ScheduleLayout.Children)
            {
                if (item is ContactsList)
                {
                    lstIndexes.Add(ScheduleLayout.Children.IndexOf(item));  //add ContactsList index value to lstIndexes
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                ScheduleLayout.Children.RemoveAt(itemIndex);    //remove ContactsList by itemIndex                    
            }
            LogHelper.Logger.Info("Ends CloseContactList Function in UserControls/Schedule.cs");
        }

        private void timePckr_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            LogHelper.Logger.Info("Starts timePckr_ValueChanged Function in UserControls/Schedule.cs");
            if (e.NewValue is DateTime)
            {
                //timePckrEnd.Value = ((DateTime)e.NewValue).AddMinutes(30);
            }
            LogHelper.Logger.Info("Ends timePckr_ValueChanged Function in UserControls/Schedule.cs");
        }

        private void txtAttendees_Populating(object sender, PopulatingEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtAttendees_Populating Function in UserControls/Schedule.cs");
            string text = txtAttendees.Text.Split(',').LastOrDefault<string>();
            ////txtAttendees.ItemsSource = lstEmailAddress.Where(c => c.StartsWith(text));
            ////txtAttendees.PopulateComplete();
            LogHelper.Logger.Info("Ends txtAttendees_Populating Function in UserControls/Schedule.cs");
        }
        private void btnGlobalContacts_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnGlobalContacts_PreviewTouchUp Function in UserControls/Schedule.cs");
            LoadAttendee();
            LogHelper.Logger.Info("Ends btnGlobalContacts_PreviewTouchUp Function in UserControls/Schedule.cs");
        }

        private void txtLocation_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtLocation_TouchDown Function in UserControls/Schedule.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtLocation_TouchDown Function in UserControls/Schedule.cs");
        }

        private void txtLocation_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtLocation_TouchLeave Function in UserControls/Schedule.cs");
            Apps.OpenVirtualKeyboard();  //open virtual keyboard
            txtLocation.Focus();
            LogHelper.Logger.Info("Ends txtLocation_TouchLeave Function in UserControls/Schedule.cs");

        }
        private void txtAttendees_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtAttendees_TouchDown Function in UserControls/Schedule.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtAttendees_TouchDown Function in UserControls/Schedule.cs");
        }
        private void txtAttendees_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtAttendees_TouchLeave Function in UserControls/Schedule.cs");
            Apps.OpenVirtualKeyboard();  //open virtual keyboard
            txtAttendees.Focus();
            LogHelper.Logger.Info("Ends txtAttendees_TouchLeave Function in UserControls/Schedule.cs");
        }

        private void txtMessage_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtMessage_TouchLeave Function in UserControls/Schedule.cs");
            Apps.OpenVirtualKeyboard();  //open virtual keyboard
            txtMessage.Focus();
            LogHelper.Logger.Info("Ends txtMessage_TouchLeave Function in UserControls/Schedule.cs");
        }

        private void txtMessage_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtMessage_TouchDown Function in UserControls/Schedule.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtMessage_TouchDown Function in UserControls/Schedule.cs");
        }

        private void ScheduleLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
        
        private void timePckr_TouchLeave(object sender, TouchEventArgs e)
        {
            if (timePckr.IsOpen)
            {
                timePckr.IsOpen = false;
            }
            else
            {
                timePckr.IsOpen = true;
            }
        }

        private void txtEvent_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDownEventRaise(sender, e);
        }

        private void KeyDownEventRaise(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                btnSave_PreviewTouchUp_1(sender, null);
            }
        }

        private void txtLocation_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDownEventRaise(sender, e);
        }

        private void txtAttendees_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDownEventRaise(sender, e);
        }
        public event EventHandler EvntUserLogout;
        private void btnLogout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntUserLogout(this, null);
        }
    }
}
