﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for SlideShow.xaml
    /// </summary>
    public partial class SlideShow : UserControl
    {
        public event EventHandler EvntCloseSlideShow;
        private DispatcherTimer timer;
        private List<string> files;
        private Configuration config;
        private int slideShowSeconds = 0;
        private int imageCount = 0;

        public SlideShow()
        {
            InitializeComponent();
        }

        private void MainLayout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (timer != null)
                timer.Stop();
            EvntCloseSlideShow(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            files = new List<string>();
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (timer != null)
                timer.Stop();

            string executedLocation = Assembly.GetEntryAssembly().Location;
            executedLocation = executedLocation.Remove(executedLocation.LastIndexOf('\\'));

            if (Directory.Exists(executedLocation + "\\SlideShowImages"))
            {
                files = Directory.GetFiles(executedLocation + "\\SlideShowImages").ToList();
            }
            else
            {
                Directory.CreateDirectory(executedLocation + "\\SlideShowImages");
            }

            if (!(Int32.TryParse(config.AppSettings.Settings["SlideShowTimer"].Value, out slideShowSeconds) && slideShowSeconds <= 120))
            {
                slideShowSeconds = 120;
            }

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, slideShowSeconds);
            timer.Tick += new EventHandler(timer_Tick);
            timer.IsEnabled = true;
            timer.Start();
            PlaySlideShow(imageCount++);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            long totalImagesCount = files.Count;

            if (imageCount >= totalImagesCount)
            {
                imageCount = 0;
            }
            PlaySlideShow(imageCount);
            imageCount++;
        }

        private void PlaySlideShow(int imgIndex)
        {
            try
            {
                if (files.Count >= imgIndex)
                {
                    string filename = files[imgIndex];
                    ImgSlideShow.Source = new BitmapImage(new Uri(filename));
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException(ex.Message, ex);
            }
        }
    }
}