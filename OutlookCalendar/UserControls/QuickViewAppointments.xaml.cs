﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for QuickViewAppointments.xaml
    /// </summary>
    public partial class QuickViewAppointments : UserControl
    {
        public QuickViewAppointments()
        {
            InitializeComponent();
        }
        private void svScheduleMeeting_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
