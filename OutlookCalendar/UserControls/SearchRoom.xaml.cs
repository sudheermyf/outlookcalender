﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for SearchRoom.xaml
    /// </summary>
    public partial class SearchRoom : Window
    {
        public SearchRoom()
        {
            InitializeComponent();
        }

        private void Close_TouchDown(object sender, TouchEventArgs e)
        {
            this.Close();
        }
    }
}
