﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for AppointmentPopUp.xaml
    /// </summary>
    public partial class AppointmentPopUp : UserControl
    {
        public AppointmentPopUp()
        {
            InitializeComponent();
        }
        public event EventHandler EvntClosePopUp;
        private void btnClosePopUp_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnClosePopUp_PreviewTouchUp Function in UserControls/AppointmentPopUp.cs");
            EvntClosePopUp(this, null);
            LogHelper.Logger.Info("Ends btnClosePopUp_PreviewTouchUp Function in UserControls/AppointmentPopUp.cs");
        }
    }
}
