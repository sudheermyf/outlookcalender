﻿using OutlookCalendar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Media.Animation;
using Microsoft.Exchange.WebServices.Data;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for ReSchedule.xaml
    /// </summary>
    public partial class ReSchedule : UserControl
    {
        public Appointment SelectedAppointment { get; set; }
        public event EventHandler modifiedAppointment;
        public event EventHandler closeReSchedule;
        public event EventHandler releaseAppointment;
        
        public ReSchedule()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
        }
        private void btnCloseReSchedule_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnEdit_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnCancel_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        public event EventHandler cancelMeeting;
        private void btnCancelReSchedule_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnExtend_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnCloseExtndDuration_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnSaveExtendDuration_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnOkCancelReSchdl_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnRelease_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnUpdate_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnCancelUpdate_PreviewTouchUp_1(object sender, TouchEventArgs e)
        { }

        private void btnReleaseOk_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnCheckReleaseClose_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnCheckIn_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void btnCheckOut_PreviewTouchUp_1(object sender, TouchEventArgs e)
        { }

        Process proc;
        private void txtEvent_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        private void txtAttendees_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
        }

        public event EventHandler reScheduleToCalendar;
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {

        }
    }
}
