﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar.UserControls.AlertMessageControls
{
    /// <summary>
    /// Interaction logic for ErrorAlert.xaml
    /// </summary>
    public partial class ErrorAlert : UserControl
    {
        public Timer delayTimer;
        public ErrorAlert()
        {
            LogHelper.Logger.Info("Starts ErrorAlert Function in AlertMessageControls/ErrorAlert.cs");
            InitializeComponent();
            delayTimer = new Timer();
            delayTimer.Interval = 3000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
            LogHelper.Logger.Info("Ends ErrorAlert Function in AlertMessageControls/ErrorAlert.cs");
        }
        public event EventHandler EvntCloseErrorAlert;
        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            LogHelper.Logger.Info("Starts delayTimer_Elapsed Function in AlertMessageControls/ErrorAlert.cs");
            this.Dispatcher.Invoke((Action)(() =>
            {
                Storyboard CloseAlert_SB = TryFindResource("CloseAlert_SB") as Storyboard;
                CloseAlert_SB.Begin();
                CloseAlert_SB.Completed -= CloseAlert_SB_Completed;
                CloseAlert_SB.Completed += CloseAlert_SB_Completed;
            }));
            delayTimer.Stop();
            LogHelper.Logger.Info("Ends delayTimer_Elapsed Function in AlertMessageControls/ErrorAlert.cs");
        }
        void CloseAlert_SB_Completed(object sender, EventArgs e)
        {
            Storyboard closeAlert = sender as Storyboard;
            if (closeAlert != null)
            {
                closeAlert.Remove();
            }

            LogHelper.Logger.Info("Starts CloseAlert_SB_Completed Function in AlertMessageControls/ErrorAlert.cs");
            EvntCloseErrorAlert(this, null);
            LogHelper.Logger.Info("Ends CloseAlert_SB_Completed Function in AlertMessageControls/ErrorAlert.cs");
        }
    }
}
