﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Timers;

namespace OutlookCalendar.UserControls.AlertMessageControls
{
    /// <summary>
    /// Interaction logic for SuccessAlert.xaml
    /// </summary>
    public partial class SuccessAlert : UserControl
    {
        public Timer delayTimer;
        public SuccessAlert()
        {
            LogHelper.Logger.Info("Starts SuccessAlert Function in AlertMessageControls/SuccessAlert.cs");
            InitializeComponent();
            delayTimer = new Timer();
            delayTimer.Interval = 3000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
            LogHelper.Logger.Info("Ends SuccessAlert Function in AlertMessageControls/SuccessAlert.cs");
        }
        public event EventHandler EvntCloseSuccessAlert;
        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            LogHelper.Logger.Info("Starts delayTimer_Elapsed Function in AlertMessageControls/SuccessAlert.cs");
            this.Dispatcher.Invoke((Action)(() =>
                {
                    Storyboard CloseAlert_SB = TryFindResource("CloseAlert_SB") as Storyboard;
                    CloseAlert_SB.Begin();
                    CloseAlert_SB.Completed -= CloseAlert_SB_Completed;
                    CloseAlert_SB.Completed += CloseAlert_SB_Completed;
                }));
            delayTimer.Stop();
            LogHelper.Logger.Info("Ends delayTimer_Elapsed Function in AlertMessageControls/SuccessAlert.cs");
        }
        void CloseAlert_SB_Completed(object sender, EventArgs e)
        {
            Storyboard closeAlert = sender as Storyboard;
            if (closeAlert != null)
            {
                closeAlert.Remove();
            }

            LogHelper.Logger.Info("Starts CloseAlert_SB_Completed Function in AlertMessageControls/SuccessAlert.cs");
            EvntCloseSuccessAlert(this, null);
            LogHelper.Logger.Info("Ends CloseAlert_SB_Completed Function in AlertMessageControls/SuccessAlert.cs");
        }
    }
}
