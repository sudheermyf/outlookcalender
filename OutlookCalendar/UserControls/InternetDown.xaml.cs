﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for InternetDown.xaml
    /// </summary>
    public partial class InternetDown : Window
    {
        DispatcherTimer timer;
        public InternetDown()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 3);
            timer.Tick += idleTimer_Tick;
            timer.Start();
        }
        void idleTimer_Tick(object sender, EventArgs e)
        {
            if(Apps.PingCheck())
            {
                timer.Stop();
                this.Close();
            }
        }
    }
}
