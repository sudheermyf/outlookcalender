﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for Loading.xaml
    /// </summary>
    public partial class Loading : UserControl
    {
        public Loading()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseLoading;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts UserControl_Loaded Function in UserControls/Loading.cs");
            Storyboard loading_SB = TryFindResource("loading_SB") as Storyboard;
            loading_SB.Completed += new EventHandler(loading_SB_Completed);
            loading_SB.Begin();
            LogHelper.Logger.Info("Ends UserControl_Loaded Function in UserControls/Loading.cs");
        }

        void loading_SB_Completed(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts loading_SB_Completed Function in UserControls/Loading.cs");
            EvntCloseLoading(this, null); // close loading event
            LogHelper.Logger.Info("Ends loading_SB_Completed Function in UserControls/Loading.cs");
        }
    }
}
