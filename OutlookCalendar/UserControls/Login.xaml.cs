﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Office365Communication;
using Microsoft.Exchange.WebServices.Data;
using System.Windows.Media.Animation;
using System.ComponentModel;
using DRLPortalDB.Models;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public event EventHandler EvntCloseLogin;
        Process proc;
        string Message;
        string processName;
        public event EventHandler EvntErrorAlert;
        public BackgroundWorker bgWorker;
        private string LoginID = string.Empty;
        private string Password = string.Empty;
        public string LoginSuccessMessage { get; set; }
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnLogin_PreviewTouchUp Function in UserControls/Login.cs");
            AuthenticateUser();
            LogHelper.Logger.Info("Ends btnLogin_PreviewTouchUp Function in UserControls/Login.cs");
        }

        private void AuthenticateUser()
        {
            LoginID = string.Empty;
            Password = string.Empty;
            if ((string.IsNullOrEmpty(txtLoginID.Text.Trim()) && string.IsNullOrEmpty(txtPwd.Password)) || !string.IsNullOrEmpty(txtRFID.Text.Trim()))
            {
                RFIDDetail rfDetail = Apps.dbContext.RFID.FirstOrDefault(c => c.RFID.Equals(txtRFID.Text.Trim()));
                if (rfDetail != null)
                {
                    LoginID = rfDetail.USERNAME;
                    Password = rfDetail.PASSWORD;
                }
                else
                {
                    Message = "RFID not exist.";
                    EvntErrorAlert(Message, null);
                    LogHelper.Logger.Info("Error : RFID not exist. Authenticate User Function in UserControls/Login.cs");
                }
            }
            else if (!string.IsNullOrEmpty(txtLoginID.Text.Trim()) && !string.IsNullOrEmpty(txtPwd.Password))
            {
                LoginID = txtLoginID.Text;
                Password = txtPwd.Password;
            }

            //CloseOSK(); //to close C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe process
            if (!string.IsNullOrEmpty(LoginID) && !string.IsNullOrEmpty(Password))  //check null or empty LoginID & Password
            {
                try
                {
                    if (bgWorker != null && !bgWorker.IsBusy)
                    {
                        bgWorker.RunWorkerAsync();
                    }
                }
                catch (Exception ex)
                {
                    Message = "Authentication Failed.";
                    EvntErrorAlert(Message, null);
                    LogHelper.Logger.Info("Error : " + ex.Message + " Authenticate User Function in UserControls/Login.cs");
                }
            }
            else
            {
                Message = "Required fields are empty.";
                EvntErrorAlert(Message, null);
                LogHelper.Logger.Info("Error : Required fields are empty. Authenticate User Function in UserControls/Login.cs");
            }
        }

        void CloseLogin_SB_Completed(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts CloseLogin_SB_Completed Function in UserControls/Login.cs");
            EvntCloseLogin(this, null);
            LogHelper.Logger.Info("Ends CloseLogin_SB_Completed Function in UserControls/Login.cs");
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts UserControl_Loaded_1 Function in UserControls/Login.cs");
            txtRFID.Text = string.Empty;
            DateTime today = DateTime.Now.Date;
            lblTodayDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.ProgressChanged += bgWorker_ProgressChanged;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            LogHelper.Logger.Info("Ends UserControl_Loaded_1 Function in UserControls/Login.cs");
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_ProgressChanged Function in UserControls/Login.cs");
            if (e.UserState is Exception)
            {
                LoginSuccessMessage = ((Exception)e.UserState).Message;
            }
            else
            {
                txtMessage.Text = Convert.ToString(e.UserState);
            }
            LogHelper.Logger.Info("Ends bgWorker_ProgressChanged Function in UserControls/Login.cs");
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_RunWorkerCompleted Function in UserControls/Login.cs");
            if (LoginSuccessMessage.Equals("Success"))
            {
                txtMessage.Text = string.Empty;
                Storyboard CloseLogin_SB = TryFindResource("CloseLogin_SB") as Storyboard;
                CloseLogin_SB.Completed += new EventHandler(CloseLogin_SB_Completed);
                CloseLogin_SB.Begin();
            }
            else
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    Storyboard closeloading_SB = TryFindResource("closeloading_SB") as Storyboard;
                    closeloading_SB.Begin();
                    txtLoginID.Text = string.Empty;
                    txtPwd.Password = string.Empty;
                }));
                EvntErrorAlert(LoginSuccessMessage, null);
            }
            LogHelper.Logger.Info("Ends bgWorker_RunWorkerCompleted Function in UserControls/Login.cs");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_DoWork Function in UserControls/Login.cs");
            LoginSuccessMessage = string.Empty;
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                    {
                        Storyboard loading_SB = TryFindResource("loading_SB") as Storyboard;
                        loading_SB.Begin();
                    }));
                bgWorker.ReportProgress(1, "Please wait... Authenticating");
                ExchangeService service = Authentication.AuthenticateService(LoginID, Password);
                Apps.service = service;
                LoginSuccessMessage = "Success";
            }
            catch (Exception ex)
            {
                LoginSuccessMessage = ex.Message;
                bgWorker.ReportProgress(1, ex);
                LogHelper.Logger.Info("Error : " + ex.Message + " bgWorker_DoWork Function in UserControls/Login.cs");
            }
            LogHelper.Logger.Info("Ends bgWorker_DoWork Function in UserControls/Login.cs");
        }

        private void txtLoginID_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtLoginID_PreviewTouchUp_1 Function in UserControls/Login.cs");
            Apps.OpenVirtualKeyboard();
            LogHelper.Logger.Info("Ends txtLoginID_PreviewTouchUp_1 Function in UserControls/Login.cs");
        }
        private void txtPwd_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtPwd_PreviewTouchUp_1 Function in UserControls/Login.cs");
            Apps.OpenVirtualKeyboard();
            LogHelper.Logger.Info("Ends txtPwd_PreviewTouchUp_1 Function in UserControls/Login.cs");
        }
        //private void OpenVirtualKeyboard()
        //{
        //    LogHelper.Logger.Info("Starts OpenVirtualKeyboard Function in UserControls/Login.cs");
        //    try
        //    {
        //        //CloseOSK();
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //        EvntErrorAlert(ex.Message, null);
        //        LogHelper.Logger.Info("Error : " + ex.Message + " OpenVirtualKeyboard Function in UserControls/Login.cs");
        //    }
        //    LogHelper.Logger.Info("Ends OpenVirtualKeyboard Function in UserControls/Login.cs");
        //}

        //private void CloseOSK()
        //{
        //    LogHelper.Logger.Info("Starts CloseOSK Function in UserControls/Login.cs");
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //    LogHelper.Logger.Info("Ends CloseOSK Function in UserControls/Login.cs");
        //}

        private void txtRFID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                txtLoginID.Text = string.Empty;
                txtPwd.Password = string.Empty;
                AuthenticateUser();
            }
        }
    }
}
