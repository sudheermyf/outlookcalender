﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace OutlookCalendar.UserControls.ScheduleControls
{
    /// <summary>
    /// Interaction logic for ContactsList.xaml
    /// </summary>
    public partial class ContactsList : UserControl
    {
        ExchangeService service;
        Process proc;
        private string processName;
        public string searchText;
        public static string selectedList;
        public event EventHandler EvntCloseContactList;
        public event EventHandler EvntAddContactList;
        public ContactsList()
        {
            InitializeComponent();
        }
        private void svContactList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts UserControl_Loaded Function in UserControls.ScheduleControls/ContactsList.cs");
            txtSearch.Focus();
            LogHelper.Logger.Info("ends UserControl_Loaded Function in UserControls.ScheduleControls/ContactsList.cs");
        }
        private void chkContactList_Checked(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts chkContactList_Checked Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.KillOSK();
            CheckBox chk = (CheckBox)sender;
            selectedList += chk.Content + ",";
            LogHelper.Logger.Info("ends chkContactList_Checked Function in UserControls.ScheduleControls/ContactsList.cs");
        }
        private void chkContactList_Unchecked(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts chkContactList_Unchecked Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.KillOSK();
            CheckBox chkUncheck = (CheckBox)sender;
            string uncheckValue = chkUncheck.Content.ToString()+",";
            selectedList = selectedList.Replace(uncheckValue, "");
            LogHelper.Logger.Info("ends chkContactList_Unchecked Function in UserControls.ScheduleControls/ContactsList.cs");
        }

        private void btnSearchContacts_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnSearchContacts_PreviewTouchUp Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.KillOSK();
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;

            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                searchText = txtSearch.Text.Trim();
                if (bgWorker != null && !bgWorker.IsBusy)
                {
                    bgWorker.RunWorkerAsync();
                }
                //service = Apps.service;
                //NameResolutionCollection collection = Office365Communication.Contacts.O365Contacts.GetGlobalContacts(service, txtSearch.Text);
                //ContactList.ItemsSource = collection;
            }
            LogHelper.Logger.Info("ends btnSearchContacts_PreviewTouchUp Function in UserControls.ScheduleControls/ContactsList.cs");
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RemoveLoadingScreen();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                Apps.PingTest();
                Loading loading = new Loading();
                Panel.SetZIndex(loading, 9999);
                loading.BringIntoView();
                loading.txtBlckLoading.Text = "Fetching Global Contacts... Please wait...";
                GrdContactListPanel.Children.Add(loading);
            }));

            service = Apps.service;
            NameResolutionCollection collection = Office365Communication.Contacts.O365Contacts.GetGlobalContacts(service, searchText);
            this.Dispatcher.Invoke((Action)(() =>
                    {
                        ContactList.ItemsSource = collection;
                    }));
        }

        private void RemoveLoadingScreen()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdContactListPanel.Children)
            {
                if (item is Loading)
                {
                    lstIndexes.Add(GrdContactListPanel.Children.IndexOf(item));
                }
            }

            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                GrdContactListPanel.Children.RemoveAt(itemIndex);
            }
        }

        private void txtSearch_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtSearch_TouchDown Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("ends txtSearch_TouchDown Function in UserControls.ScheduleControls/ContactsList.cs");
        }
        private void txtSearch_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtSearch_TouchLeave Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.OpenVirtualKeyboard();
            LogHelper.Logger.Info("ends txtSearch_TouchLeave Function in UserControls.ScheduleControls/ContactsList.cs");
        }
        //private void OpenVirtualKeyboard()
        //{
        //    LogHelper.Logger.Info("Starts OpenVirtualKeyboard Function in UserControls.ScheduleControls/ContactsList.cs");
        //    try
        //    {
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    LogHelper.Logger.Info("Ends OpenVirtualKeyboard Function in UserControls.ScheduleControls/ContactsList.cs");
        //}

        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void ContactList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Apps.KillOSK();
        }

        private void btnCloseContactList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnCloseContactList_PreviewTouchUp Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.KillOSK();
            EvntCloseContactList(this, null);
            LogHelper.Logger.Info("ends btnCloseContactList_PreviewTouchUp Function in UserControls.ScheduleControls/ContactsList.cs");
        }
        private void btnSaveList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnSaveList_PreviewTouchUp Function in UserControls.ScheduleControls/ContactsList.cs");
            Apps.KillOSK();
            selectedList = string.Empty;
            List<string> lstEmailAddr = new List<string>();
            foreach (var item in ContactList.SelectedItems)
            {
                NameResolution res = item as NameResolution;
                if (res != null && res.Mailbox != null)
                {
                    lstEmailAddr.Add(res.Mailbox.Address);
                }
            }

            lstEmailAddr.ForEach(c =>
            {
                selectedList += c + ",";
            });
            EvntAddContactList(selectedList, null);
            LogHelper.Logger.Info("ends btnSaveList_PreviewTouchUp Function in UserControls.ScheduleControls/ContactsList.cs");
        }
    }
}
