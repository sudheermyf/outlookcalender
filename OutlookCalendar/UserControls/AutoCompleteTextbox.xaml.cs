﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OutlookCalendar.Model;

namespace OutlookCalendar.UserControls
{
    /// <summary>
    /// Interaction logic for AutoCompleteTextbox.xaml
    /// </summary>
    public partial class AutoCompleteTextbox : Canvas
    {
        
        #region Members
        private VisualCollection controls;
        private TextBox textBox;
        private ComboBox comboBox;
        private ObservableCollection<AutoCompleteEntry> autoCompletionList;
        private System.Timers.Timer keypressTimer;
        private delegate void TextChangedCallback();
        private bool insertText;
        private int delayTime;
        private int searchThreshold;
        #endregion

        #region Constructor
        public AutoCompleteTextbox()
        {
            LogHelper.Logger.Info("Starts AutoCompleteTextbox Function in UserControls/AutoCompleteTextbox.cs");
            controls = new VisualCollection(this);
            InitializeComponent();

            autoCompletionList = new ObservableCollection<AutoCompleteEntry>();
            searchThreshold = 1;        // default threshold to 1 char

            // set up the key press timer
            keypressTimer = new System.Timers.Timer();
            keypressTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);

            //this.TouchDown += AutoCompleteTextbox_TouchDown;

            // set up the text box and the combo box
            comboBox = new ComboBox();
            comboBox.IsSynchronizedWithCurrentItem = true;
            comboBox.IsTabStop = false;
            comboBox.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);

            textBox = new TextBox();
            textBox.TextChanged += new TextChangedEventHandler(textBox_TextChanged);
            textBox.VerticalContentAlignment = VerticalAlignment.Center;

            controls.Add(comboBox);
            controls.Add(textBox);
            LogHelper.Logger.Info("Ends AutoCompleteTextbox Function in UserControls/AutoCompleteTextbox.cs");
        }

        void AutoCompleteTextbox_TouchDown(object sender, TouchEventArgs e)
        {
            //textBox.BringIntoView();
            textBox.Focus();
        }
        #endregion

        #region Methods
        public string Text
        {
            get { return textBox.Text; }
            set 
            {
                insertText = true;
                textBox.Text = value; 
            }
        }

        public int DelayTime
        {
            get { return delayTime; }
            set { delayTime = value; }
        }

        public int Threshold
        {
            get { return searchThreshold; }
            set { searchThreshold = value; }
        }

        public void AddItem(AutoCompleteEntry entry)
        {
            autoCompletionList.Add(entry);            
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LogHelper.Logger.Info("Starts comboBox_SelectionChanged Function in UserControls/AutoCompleteTextbox.cs");
            if (null != comboBox.SelectedItem)
            {
                insertText = true;
                ComboBoxItem cbItem = (ComboBoxItem)comboBox.SelectedItem;
                textBox.Text = textBox.Text.Remove(textBox.Text.LastIndexOf(",") + 1) + cbItem.Content.ToString() + ", ";
            }
            LogHelper.Logger.Info("Ends comboBox_SelectionChanged Function in UserControls/AutoCompleteTextbox.cs");
        }

        private void TextChanged()
        {
            LogHelper.Logger.Info("Starts TextChanged Function in UserControls/AutoCompleteTextbox.cs");
            try
            {
                 comboBox.Items.Clear();
                if (textBox.Text.Length >= searchThreshold)
                {
                    string[] lstAttendee = textBox.Text.Split(',');
                    var lastAttendee = lstAttendee.Last().TrimStart().TrimEnd();                    
                    foreach (AutoCompleteEntry entry in autoCompletionList)
                    {
                        foreach (string word in entry.KeywordStrings)
                        {
                            //if (word.StartsWith(textBox.Text, StringComparison.CurrentCultureIgnoreCase))
                            if (word.StartsWith(lastAttendee, StringComparison.CurrentCultureIgnoreCase))
                            {
                                ComboBoxItem cbItem = new ComboBoxItem();
                                cbItem.Content = entry.ToString();
                                comboBox.Items.Add(cbItem);
                                break;
                            }
                        }
                    }
                    comboBox.IsDropDownOpen = comboBox.HasItems;                    
                }
                else
                {
                    comboBox.IsDropDownOpen = false;
                }
            }
            catch { }
            LogHelper.Logger.Info("Ends TextChanged Function in UserControls/AutoCompleteTextbox.cs");
        }

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            LogHelper.Logger.Info("Starts OnTimedEvent Function in UserControls/AutoCompleteTextbox.cs");
            keypressTimer.Stop();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                new TextChangedCallback(this.TextChanged));
            LogHelper.Logger.Info("Ends OnTimedEvent Function in UserControls/AutoCompleteTextbox.cs");
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            LogHelper.Logger.Info("Starts textBox_TextChanged Function in UserControls/AutoCompleteTextbox.cs");
            // text was not typed, do nothing and consume the flag
            if (insertText == true) insertText = false;
            
            // if the delay time is set, delay handling of text changed
            else
            {
                if (delayTime > 0)
                {
                    keypressTimer.Interval = delayTime;
                    keypressTimer.Start();
                }
                else TextChanged();
            }
            LogHelper.Logger.Info("Ends textBox_TextChanged Function in UserControls/AutoCompleteTextbox.cs");
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            LogHelper.Logger.Info("Starts ArrangeOverride Function in UserControls/AutoCompleteTextbox.cs");
            textBox.Arrange(new Rect(arrangeSize));
            comboBox.Arrange(new Rect(arrangeSize));
            LogHelper.Logger.Info("Ends ArrangeOverride Function in UserControls/AutoCompleteTextbox.cs");
            return base.ArrangeOverride(arrangeSize);
        }

        protected override Visual GetVisualChild(int index)
        {
            return controls[index];
        }

        protected override int VisualChildrenCount
        {
            get { return controls.Count; }
        }
        #endregion

        
    }
}
