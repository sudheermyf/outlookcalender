﻿using Microsoft.Exchange.WebServices.Data;
using Office365Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookCalendar.Logic
{
    public class O365Autenticate
    {
        public bool AuthenticateUser(string email, string password, out string result)
        {
            result = string.Empty;
            if ((!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password)))
            {
                try
                {
                    ExchangeService service = Authentication.AuthenticateService(email, password);
                    Apps.service = service;
                    Apps.USERNAME = email;
                    Apps.FromAutoLogin = false;
                    return true;
                }
                catch (Exception ex)
                {
                    result = "Authentication Failed.";
                    LogHelper.Logger.Info("Error : " + ex.Message + " Authenticate User Function in DRLLogin.xaml.cs");
                    return false;
                }
            }
            else
            {
                result = "Required fields are empty.";
                LogHelper.Logger.Info("Error : Required fields are empty. Authenticate User Function in DRLLogin.xaml.cs");
                return false;
            }
        }
    }
}