﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Subsembly.SmartCard;
using System.Diagnostics;
using OutlookCalendar.Model;

namespace OutlookCalendar.Logic
{
    public class InitiateSmartCard
    {
        public event EventHandler refreshValues;
        public InitiateSmartCard()
        {
            #region Initiate Smart Card Reader
            CardTerminalManager.Singleton.CardInsertedEvent +=
                new CardTerminalEventHandler(InsertedEvent);
            //CardTerminalManager.Singleton.CardTerminalLostEvent +=
            //    new CardTerminalEventHandler(TerminalLostEvent);
            CardTerminalManager.Singleton.CardTerminalFoundEvent +=
                new CardTerminalEventHandler(TerminalFoundEvent);
            #endregion
        }

        public string DetechSmartCardReader()
        {
            try
            {
                Apps.smartCardReaderValues = new SmartCardReaderValues();
                CardTerminalManager.Singleton.Startup(true);
                if (CardTerminalManager.Singleton.SlotCount == 0)
                {
                    Apps.smartCardReaderValues.ErrorMessage = "No reader available";
                    return Apps.smartCardReaderValues.ErrorMessage;
                    refreshValues(this, null);
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return string.Empty;
        }

        public void InsertedEvent(object aSender, CardTerminalEventArgs aEventArgs)
        {
            try
            {
                Apps.smartCardReaderValues = new SmartCardReaderValues();
                string localErrorMessage = string.Empty;
                //Clearing RFID Textbox before going to read id
                Apps.smartCardReaderValues.Rfid = InitiateSmartCard.ReadCard(aEventArgs.Slot, out localErrorMessage);
                Apps.smartCardReaderValues.ErrorMessage = localErrorMessage;
                refreshValues(this, null);
            }
            catch(Exception ex)
            {
                Apps.smartCardReaderValues.ErrorMessage = ex.Message;
            }
        }

        private void TerminalFoundEvent(object aSender, CardTerminalEventArgs aEventArgs)
        {
            if (CardTerminalManager.Singleton.StartedUp)
            {
                Apps.smartCardReaderValues = new SmartCardReaderValues();
                Apps.smartCardReaderValues.ErrorMessage = "Insert card ...";
                refreshValues(this, null);
            }
        }

        private void TerminalLostEvent(object aSender, CardTerminalEventArgs aEventArgs)
        {
            if (CardTerminalManager.Singleton.StartedUp)
            {
                CardTerminalManager.Singleton.DelistCardTerminal(aEventArgs.Slot.CardTerminal);

                //if (CardTerminalManager.Singleton.SlotCount == 0)
                {
                    Apps.smartCardReaderValues = new SmartCardReaderValues();
                    Apps.smartCardReaderValues.ErrorMessage = "Connect reader ...";
                    refreshValues(this, null);
                }
            }
        }

        public static string ReadCard(CardTerminalSlot aCardSlot,out string errorMessage)
        {
            errorMessage = string.Empty;
            string result = string.Empty;
            string readerName = aCardSlot.CardTerminalName;

            CardActivationResult nActivationResult;
            CardHandle aCard = aCardSlot.AcquireCard((CardTypes.T0 | CardTypes.T1), out nActivationResult);
            if (nActivationResult != CardActivationResult.Success)
            {
                switch (nActivationResult)
                {
                    case CardActivationResult.NoCard:
                        errorMessage = readerName + ": Please insert card ...";
                        break;
                    case CardActivationResult.UnresponsiveCard:
                        errorMessage = readerName + ": Unresponsive card.";
                        break;
                    case CardActivationResult.InUse:
                        errorMessage = readerName + ": Card in use";
                        break;
                    default:
                        errorMessage = readerName + ": Can't power up card!";
                        break;
                }
            }
            aCardSlot.BeginTransaction();
            byte[] atr = aCard.GetATR();
            if (atr.Length == 0)
            {
                errorMessage = "Invalid ATR";
            }
            else
            {
                result = CardHex.FromByteArray(atr, 0, atr.Length);
            }

            string uidCode = FetchUID(aCard);

            if (string.IsNullOrEmpty(uidCode))
            {
                errorMessage = "Failed reading UID. Please contact card support.";
            }

            aCardSlot.EndTransaction();
            return result + ":" + uidCode;
        }

        private static string FetchUID(CardHandle aCard)
        {
            string uidCode = string.Empty;
            HelloCardHelper cardHelper = new HelloCardHelper(aCard);
            if (cardHelper.isContactless)
            {
                byte CL_CLA = 0xFF;
                byte CL_INS_GET_UID = 0xCA;
                byte P1 = 0;
                byte P2 = 0;
                CardCommandAPDU aCmdAPDU = new CardCommandAPDU(CL_CLA, CL_INS_GET_UID, P1, P2, 256);
                CardResponseAPDU aRespAPDU;
                aRespAPDU = aCard.SendCommand(aCmdAPDU);
                if (!aRespAPDU.IsSuccessful)
                {
                    uidCode= string.Empty;
                }
                else
                {
                    byte[] uidWithSw12 = aRespAPDU.GenerateBytes();
                    if (uidWithSw12.Length < 2) throw new Exception("Invalid UID");
                    uidCode = CardHex.FromByteArray(uidWithSw12, 0, uidWithSw12.Length - 2);
                }
            }
            return uidCode;
        }
    }
}
