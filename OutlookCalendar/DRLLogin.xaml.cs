﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Office365Communication;
using Microsoft.Exchange.WebServices.Data;
using System.Windows.Media.Animation;
using System.ComponentModel;
using DRLPortalDB.Models;
using OutlookCalendar.LoginUserControls;
using OutlookCalendar.Logic;


namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for DRLLogin.xaml
    /// </summary>
    public partial class DRLLogin : Window
    {
        string Message;
        public event EventHandler EvntErrorAlert;
        //public event EventHandler EvntCloseLogin;
        public BackgroundWorker bgWorker;
        private string LoginID = string.Empty;
        private string Password = string.Empty;
        public string readRFID { get; set; }
        public string LoginSuccessMessage { get; set; }
        public DRLLogin()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts UserControl_Loaded_1 Function in DRLLogin.xaml.cs");
            //this.DataContext = Apps.smartCardReaderValues;
            txtRFID.Password = string.Empty;
            txtRFID.Focus();
            DateTime today = DateTime.Now.Date;
            lblTodayDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.ProgressChanged += bgWorker_ProgressChanged;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            LogHelper.Logger.Info("Ends UserControl_Loaded_1 Function in DRLLogin.xaml.cs");
        }
        private void btnLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnLogin_PreviewTouchUp Function in DRLLogin.xaml.cs");
            Apps.KillOSK();
            AuthenticateUser();
            LogHelper.Logger.Info("Ends btnLogin_PreviewTouchUp Function in DRLLogin.xaml.cs");
        }
        private void AuthenticateUser()
        {
            LoginID = string.Empty;
            Password = string.Empty;
            if (!string.IsNullOrEmpty(txtRFID.Password.Trim()))
            {
                ADUserInfo rfDetail = Apps.dbContext.ADUSERINFO.FirstOrDefault(c => c.RFID.Equals(txtRFID.Password.Trim()));
                if (rfDetail != null)
                {
                    LoginID = rfDetail.EMAILADDRESS;
                    Password = Security.EncryptAndDecrypt.Decrypt(rfDetail.ENCRYPTEDPWD);
                    Apps.USERNAME = rfDetail.EMAILADDRESS;
                }
                else
                {
                    LogHelper.Logger.Info("Error : RFID not exist in AD. Redirecting to Register RFID with AD.");
                    LoadRFIDUserControl();
                }
            }
            else if (!string.IsNullOrEmpty(txtLoginID.Text.Trim()) && !string.IsNullOrEmpty(txtPwd.Password))
            {
                LoginID = txtLoginID.Text;
                Password = txtPwd.Password;
            }
            else
            {
                Message = "Required fields are empty.";
                MessageBox.Show(Message, "DRL", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.Logger.Info("Error : Required fields are empty. Authenticate User Function in DRLLogin.xaml.cs");
            }

             //to close C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe process
            if (!string.IsNullOrEmpty(LoginID) && !string.IsNullOrEmpty(Password))  //check null or empty LoginID & Password
            {
                try
                {
                    if (bgWorker != null && !bgWorker.IsBusy)
                    {
                        bgWorker.RunWorkerAsync();
                    }
                }
                catch (Exception ex)
                {
                    Message = "Authentication Failed.";
                    EvntErrorAlert(Message, null);
                    LogHelper.Logger.Info("Error : " + ex.Message + " Authenticate User Function in DRLLogin.xaml.cs");
                }
            }
        }
        void CloseLogin_SB_Completed(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts CloseLogin_SB_Completed Function in DRLLogin.xaml.cs");
            //EvntCloseLogin(this, null);
            this.Close();
            LogHelper.Logger.Info("Ends CloseLogin_SB_Completed Function in DRLLogin.xaml.cs");
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_ProgressChanged Function in DRLLogin.xaml.cs");
            if (e.UserState is Exception)
            {
                LoginSuccessMessage = ((Exception)e.UserState).Message;
            }
            else
            {
                txtMessage.Text = Convert.ToString(e.UserState);
            }
            LogHelper.Logger.Info("Ends bgWorker_ProgressChanged Function in DRLLogin.xaml.cs");
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_RunWorkerCompleted Function in DRLLogin.xaml.cs");
            if (LoginSuccessMessage.Equals("Success"))
            {
                txtMessage.Text = string.Empty;
                Apps.FromAutoLogin = false;
                Apps.USERNAME = txtLoginID.Text;
                Window1 win1 = new Window1();
                win1.txtRoomName.Text = Apps.USERNAME;
                this.Close();
                //Storyboard CloseLogin_SB = TryFindResource("CloseLogin_SB") as Storyboard;
                //CloseLogin_SB.Completed += new EventHandler(CloseLogin_SB_Completed);
                //CloseLogin_SB.Begin();
            }
            else
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    this.Close();
                    //Storyboard closeloading_SB = TryFindResource("closeloading_SB") as Storyboard;
                    //closeloading_SB.Begin();
                    txtLoginID.Text = string.Empty;
                    txtPwd.Password = string.Empty;
                }));
                //EvntErrorAlert(LoginSuccessMessage, null);
            }
            LogHelper.Logger.Info("Ends bgWorker_RunWorkerCompleted Function in DRLLogin.xaml.cs");
        }
        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_DoWork Function in DRLLogin.xaml.cs");
            LoginSuccessMessage = string.Empty;
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    Storyboard loading_SB = TryFindResource("loading_SB") as Storyboard;
                    loading_SB.Begin();
                }));
                bgWorker.ReportProgress(1, "Please wait... Authenticating");
                ExchangeService service = Authentication.AuthenticateService(LoginID, Password);
                Apps.service = service;
                LoginSuccessMessage = "Success";
            }
            catch (Exception ex)
            {
                LoginSuccessMessage = ex.Message;
                bgWorker.ReportProgress(1, ex);
                LogHelper.Logger.Info("Error : " + ex.Message + " bgWorker_DoWork Function in DRLLogin.xaml.cs");
            }
            LogHelper.Logger.Info("Ends bgWorker_DoWork Function in DRLLogin.xaml.cs");
        }
        //private void OpenVirtualKeyboard()
        //{
        //    LogHelper.Logger.Info("Starts OpenVirtualKeyboard Function in DRLLogin.xaml.cs");
        //    try
        //    {
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.Logger.Info("Error : " + ex.Message + " OpenVirtualKeyboard Function in DRLLogin.xaml.cs");
        //    }
        //    LogHelper.Logger.Info("Ends OpenVirtualKeyboard Function in DRLLogin.xaml.cs");
        //}
        //private void CloseOSK()
        //{
        //    LogHelper.Logger.Info("Starts CloseOSK Function in DRLLogin.xaml.cs");
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //    LogHelper.Logger.Info("Ends CloseOSK Function in DRLLogin.xaml.cs");
        //}
        private void txtRFID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                txtLoginID.Text = string.Empty;
                txtPwd.Password = string.Empty;
                AuthenticateUser();
            }
        }

        private void LoadRFIDUserControl()
        {
            RFIDPasswordUC rfidPwd = new RFIDPasswordUC();
            rfidPwd.RFID = txtRFID.Password.Trim();
            LoginUserControlPanel.Children.Add(rfidPwd);
            //raise events from RFID User Control
            rfidPwd.EvntSaveRFIDPwd -= rfidPwd_EvntSaveRFIDPwd;
            rfidPwd.EvntCloseRFIDPwd -= rfidPwd_EvntCloseRFIDPwd;

            rfidPwd.EvntSaveRFIDPwd += rfidPwd_EvntSaveRFIDPwd;
            rfidPwd.EvntCloseRFIDPwd += rfidPwd_EvntCloseRFIDPwd;
        }

        void rfidPwd_EvntCloseRFIDPwd(object sender, EventArgs e)
        {
            RFIDPasswordUC rfidPwd = (RFIDPasswordUC)sender;
            LoginUserControlPanel.Children.Remove(rfidPwd);
            txtRFID.Password = string.Empty;
            txtRFID.Focus();
        }
        void rfidPwd_EvntSaveRFIDPwd(object sender, EventArgs e)
        {
            CloseRFIDPwdUC();
            AuthenticateUser();
        }
        private void CloseRFIDPwdUC()
        {
            List<int> lstIndexes = new List<int>(); //list to store RFIDPasswordUC index value in LoginUserControlPanel
            foreach (UIElement item in LoginUserControlPanel.Children)
            {
                if (item is RFIDPasswordUC)
                {
                    lstIndexes.Add(LoginUserControlPanel.Children.IndexOf(item));  //add RFIDPasswordUC index value to lstIndexes
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                LoginUserControlPanel.Children.RemoveAt(itemIndex);    //remove RFIDPasswordUC by itemIndex
            }
        }
        private void btnClose_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnClose_PreviewTouchUp Function in DRLLogin.xaml.cs");
            //Apps.FromAutoLogin = true;
            this.Close();
            //Storyboard CloseLogin_SB = TryFindResource("CloseLogin_SB") as Storyboard;
            //CloseLogin_SB.Completed += new EventHandler(CloseLogin_SB_Completed);
            //CloseLogin_SB.Begin();
            LogHelper.Logger.Info("Ends btnClose_PreviewTouchUp Function in DRLLogin.xaml.cs");
        }

        private void txtLoginID_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtLoginID_TouchDown Function in DRLLogin.xaml.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtLoginID_TouchDown Function in DRLLogin.xaml.cs");
        }
        private void txtLoginID_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtLoginID_TouchDown Function in DRLLogin.xaml.cs");
            Apps.OpenVirtualKeyboard();
            txtLoginID.Focus();
            LogHelper.Logger.Info("Ends txtLoginID_TouchDown Function in DRLLogin.xaml.cs");
        }
        //private void KillOSK()
        //{
        //    try
        //    {
        //        Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //        foreach (Process proc in process)
        //        {
        //            proc.Close();
        //            txtRFID.Focus();
        //        }
        //    }
        //    catch (Exception)
        //    { }
        //}
        private void txtPwd_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtPwd_TouchDown Function in DRLLogin.xaml.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtPwd_TouchDown Function in DRLLogin.xaml.cs");
        }
        private void txtPwd_TouchLeave(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtPwd_TouchLeave Function in DRLLogin.xaml.cs");
            Apps.OpenVirtualKeyboard();
            txtPwd.Focus();
            LogHelper.Logger.Info("Ends txtPwd_TouchLeave Function in DRLLogin.xaml.cs");
        }
        private void txtLoginID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                AuthenticateUser();
            }
        }

        private void txtRFID_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtRFID_TouchDown Function in DRLLogin.xaml.cs");
            txtRFID.Password = string.Empty;
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtRFID_TouchDown Function in DRLLogin.xaml.cs");
        }

        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
        public void InitiateAuthentication()
        {
            txtLoginID.Text = string.Empty;
            txtPwd.Password = string.Empty;
            AuthenticateUser();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
