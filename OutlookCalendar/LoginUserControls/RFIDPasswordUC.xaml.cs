﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DRLPortalDB.Models;
using OutlookCalendar.Logic;
using System.ComponentModel;
using OutlookCalendar.UserControls;
using OutlookCalendar.UserControls.AlertMessageControls;
using System.Windows.Media.Animation;

namespace OutlookCalendar.LoginUserControls
{
    /// <summary>
    /// Interaction logic for RFIDPasswordUC.xaml
    /// </summary>
    public partial class RFIDPasswordUC : UserControl
    {
        Process proc;
        
        private string processName;
        private string result = string.Empty;
        private bool isException = true;
        private BackgroundWorker bgWorker;

        public string RFID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public event EventHandler EvntSaveRFIDPwd;
        public event EventHandler EvntCloseRFIDPwd;

        public RFIDPasswordUC()
        {
            InitializeComponent();

            isException = true;
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
        }

        //private void OpenVirtualKeyboard()
        //{
        //    LogHelper.Logger.Info("Starts OpenVirtualKeyboard Function in RFIDPasswordUC.cs");
        //    try
        //    {
        //        //CloseOSK();
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    LogHelper.Logger.Info("Ends OpenVirtualKeyboard Function in RFIDPasswordUC.cs");
        //}
        //private void CloseOSK()
        //{
        //    try
        //    {
        //        Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //        foreach (Process proc in process)
        //        {
        //            proc.CloseMainWindow();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.Logger.ErrorException(ex.Message, ex);
        //    }
        //}

        private void ShowLoadingScreen()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                Apps.PingTest();
                Loading loading = new Loading();
                Panel.SetZIndex(loading, 9999);
                loading.BringIntoView();
                loading.txtBlckLoading.Text = "Authenticating please wait...";
                MainLayout.Children.Add(loading);
            }));
        }

        private void RemoveLoadingScreen()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in MainLayout.Children)
            {
                if (item is Loading)
                {
                    lstIndexes.Add(MainLayout.Children.IndexOf(item));
                }
            }

            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                MainLayout.Children.RemoveAt(itemIndex);
            }
        }

        private void btnSaveRFIDPwd_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            SaveDetails();
        }

        private void SaveDetails()
        {
            Apps.KillOSK();
            LogHelper.Logger.Info("Starts btnSaveRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
            if (!string.IsNullOrEmpty(txtRFIDUsername.Text) && !string.IsNullOrEmpty(txtRFIDPassword.Password))
            {
                btnSaveRFIDPwd.IsEnabled = false;
                UserName = txtRFIDUsername.Text;
                Password = txtRFIDPassword.Password;

                if (bgWorker != null && !bgWorker.IsBusy)
                {
                    bgWorker.RunWorkerAsync();
                }
            }
            else
            {
                MessageBox.Show("Please Enter Valid UserName and Password....", "DRL", MessageBoxButton.OK, MessageBoxImage.Information);
                txtRFIDUsername.Text = string.Empty;
                txtRFIDPassword.Password = string.Empty;
                btnSaveRFIDPwd.IsEnabled = true;
            }
            //Show Loading control...
            LogHelper.Logger.Info("Ends btnSaveRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RemoveLoadingScreen();
            btnSaveRFIDPwd.IsEnabled = true;
            if(!isException)
            {
                EvntSaveRFIDPwd(this, null);
            }
            else
            {
                MessageBox.Show(result, "DRL", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void errAlert_EvntCloseErrorAlert(object sender, EventArgs e)
        {
            List<int> lstIndexes = new List<int>(); //list to store Loading index value in MainLayout
            foreach (UIElement item in MainLayout.Children)
            {
                if (item is ErrorAlert || item is Loading)
                {
                    lstIndexes.Add(MainLayout.Children.IndexOf(item));  //add Loading index value to lstIndexes
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                MainLayout.Children.RemoveAt(itemIndex);    //remove Loading by itemIndex                    
            }
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ShowLoadingScreen();
                ADUserInfo fetchedUserInfo = Apps.dbContext.ADUSERINFO.FirstOrDefault(c => c.LOGINNAME.Equals(UserName.Trim()));
                if (fetchedUserInfo != null)
                {
                    result = string.Empty;
                    O365Autenticate preAuthentication = new O365Autenticate();
                    if (preAuthentication.AuthenticateUser(fetchedUserInfo.EMAILADDRESS, Password.Trim(), out result))
                    {
                        Apps.USERNAME = fetchedUserInfo.EMAILADDRESS;
                        fetchedUserInfo.ENCRYPTEDPWD = Security.EncryptAndDecrypt.Encrypt(Password.Trim());
                        fetchedUserInfo.RFID = RFID;
                        Apps.dbContext.SaveChanges();
                        isException = false;
                        result = "Success";
                    }
                }
                else
                {
                    result = "Login Name: Enter valid login name.";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
                LogHelper.Logger.ErrorException(ex.Message, ex);
            }
        }

        private void btnCloseRFIDPwd_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnCloseRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
            Apps.KillOSK();
            Storyboard CloseLogin_SB = TryFindResource("CloseLogin_SB") as Storyboard;
            CloseLogin_SB.Completed += new EventHandler(CloseLogin_SB_Completed);
            CloseLogin_SB.Begin();

            LogHelper.Logger.Info("Ends btnCloseRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
        }

        private void CloseLogin_SB_Completed(object sender, EventArgs e)
        {
            EvntCloseRFIDPwd(this, null);
        }

        private void txtRFIDUsername_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtRFIDUsername_TouchDown Function in RFIDPasswordUC.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtRFIDUsername_TouchDown Function in RFIDPasswordUC.cs");
        }

        private void txtRFIDUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                SaveDetails();
                //LogHelper.Logger.Info("Starts btnSaveRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
                ////CloseOSK();
                //Apps.KillOSK();
                //if (bgWorker != null && !bgWorker.IsBusy)
                //{
                //    bgWorker.RunWorkerAsync();
                //}
                ////Show Loading control...
                //LogHelper.Logger.Info("Ends btnSaveRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
            }
        }

        private void txtRFIDPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                SaveDetails();
                //LogHelper.Logger.Info("Starts btnSaveRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
                //Apps.KillOSK();
                //if (bgWorker != null && !bgWorker.IsBusy)
                //{
                //    bgWorker.RunWorkerAsync();
                //}
                ////Show Loading control...
                //LogHelper.Logger.Info("Ends btnSaveRFIDPwd_PreviewTouchUp Function in RFIDPasswordUC.cs");
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtRFIDUsername.Focus();
        }  
        private void txtRFIDPassword_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts txtRFIDPassword_TouchDown Function in RFIDPasswordUC.cs");
            Apps.KillOSK();
            LogHelper.Logger.Info("Ends txtRFIDPassword_TouchDown Function in RFIDPasswordUC.cs");
        }

        private void txtRFIDUsername_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            txtRFIDUsername.Focus();
        }

        private void txtRFIDPassword_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            txtRFIDPassword.Focus();
        }

        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

    }
}
