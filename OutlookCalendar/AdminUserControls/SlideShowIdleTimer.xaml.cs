﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar.AdminUserControls
{
    /// <summary>
    /// Interaction logic for SlideShowIdleTimer.xaml
    /// </summary>
    public partial class SlideShowIdleTimer : UserControl
    {
        public SlideShowIdleTimer()
        {
            InitializeComponent();
        }
        Process proc;
        string processName;
        public event EventHandler EvntCloseIdleTimer;
        private Configuration config;
        private int idleTimerSeconds;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            EvntCloseIdleTimer(this, null);
        }
        private void btnSave_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            int idleSeconds = 0;
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            }

            if(Int32.TryParse(txtTimer.Text.Trim(),out idleSeconds))
            {
                if(idleSeconds>120)
                {
                    config.AppSettings.Settings["IdleTimer"].Value = idleSeconds.ToString();
                    config.Save(ConfigurationSaveMode.Full, true);
                    EvntCloseIdleTimer(this, null);
                }
                else
                {
                    MessageBox.Show("Idle seconds must be greater than 120 seconds.", "DRL", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds) && idleTimerSeconds <= 120))
            {
                idleTimerSeconds = 120;
            }
            txtTimer.Text = idleTimerSeconds.ToString();
        }
        //private void OpenVirtualKeyboard()
        //{
        //    try
        //    {
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        //private void CloseOSK()
        //{
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //}

        private void txtTimer_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void txtTimer_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            //txtTimer.Focus();
        }

        private void GrdMainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
    }
}
