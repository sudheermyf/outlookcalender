﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar.AdminUserControls
{
    /// <summary>
    /// Interaction logic for SlideShowImages.xaml
    /// </summary>
    public partial class SlideShowImages : UserControl
    {
        public event EventHandler EvntCloseSlideShow;
        private string executedLocation = string.Empty;

        public SlideShowImages()
        {
            InitializeComponent();
        }
        private void btnUpload_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Multiselect = true;
                dlg.DefaultExt = ".png";
                dlg.Filter = "Image files (.png)|*.png";
                dlg.ShowDialog();
                if (dlg.FileNames != null && dlg.FileNames.Length > 0)
                {
                    if (string.IsNullOrEmpty(executedLocation))
                    {
                        executedLocation = Assembly.GetEntryAssembly().Location;
                        executedLocation = executedLocation.Remove(executedLocation.LastIndexOf('\\'));
                    }

                    if (!Directory.Exists(executedLocation + "\\SlideShowImages"))
                    {
                        Directory.CreateDirectory(executedLocation + "\\SlideShowImages");
                    }

                    foreach (string item in dlg.FileNames)
                    {
                        string filename = System.IO.Path.GetFileName(item);
                        File.Copy(item, executedLocation + "\\SlideShowImages\\" + filename, true);
                    }
                }
                MessageBox.Show("Upload successfully.", "DRL");
                EvntCloseSlideShow(this, null);
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException(ex.Message, ex);
                MessageBox.Show("Upload failed.", "DRL");
            }
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseSlideShow(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            executedLocation = Assembly.GetEntryAssembly().Location;
            executedLocation = executedLocation.Remove(executedLocation.LastIndexOf('\\'));
        }
    }
}
