﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.Configuration;
using System.Diagnostics;

namespace OutlookCalendar.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AutoLogin.xaml
    /// </summary>
    public partial class AutoLogin : UserControl
    {
        public AutoLogin()
        {
            InitializeComponent();
        }
        private Configuration config;
        Process proc;
        string processName;
        public event EventHandler EvntCloseAutoLogin;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            EvntCloseAutoLogin(this, null);
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            txtUserName.Text = config.AppSettings.Settings["AutoLoginUserName"].Value;
            txtPassword.Password = config.AppSettings.Settings["AutoLoginPassword"].Value;
        }
        private void btnSave_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            }
            config.AppSettings.Settings["AutoLoginUserName"].Value = txtUserName.Text;
            config.AppSettings.Settings["AutoLoginPassword"].Value = txtPassword.Password;
            config.Save(ConfigurationSaveMode.Full, true);
            MessageBox.Show("Saved Successfully", "DRL");
            EvntCloseAutoLogin(this, null);
        }
        private void OpenVirtualKeyboard()
        {
            try
            {
                proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
                processName = proc.ProcessName;
            }
            catch (Exception ex)
            {
            }
        }
        //private void CloseOSK()
        //{
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //}
        
        private void txtUserName_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void txtUserName_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            //txtUserName.Focus();
        }

        private void txtPassword_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void txtPassword_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            //txtPassword.Focus();
        }
        private void GrdMainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
    }
}
