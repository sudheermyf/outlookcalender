﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminLogin.xaml
    /// </summary>
    public partial class AdminLogin : UserControl
    {
        public AdminLogin()
        {
            InitializeComponent();
        }
        Process proc;
        string processName;
        public event EventHandler EvntLoginAdmin;
        public event EventHandler EvntCloseAdminLogin;
        private void txtPassword_TouchEnter_1(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
        private void txtPassword_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            //txtPassword.Focus();
        }
        private void txtName_TouchEnter_1(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
        private void txtName_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            //txtName.Focus();
        }
        private void btnLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (txtName.Text == "admin" && txtPassword.Password == "123")
            {
                Apps.KillOSK();
                EvntLoginAdmin(this, null);
            }
            else
            {
                MessageBox.Show("Please Enter Valid UserName and Password....", "DRL", MessageBoxButton.OK, MessageBoxImage.Information);
                txtName.Text = string.Empty;
                txtPassword.Password = string.Empty;
            }
        }
        private void btnCanelLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            EvntCloseAdminLogin(this, null);
        }

        //private void OpenVirtualKeyboard()
        //{
        //    LogHelper.Logger.Info("Starts OpenVirtualKeyboard Function in AdminUserControls/AdminLogin.cs");
        //    try
        //    {
        //        proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
        //        processName = proc.ProcessName;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    LogHelper.Logger.Info("Ends OpenVirtualKeyboard Function in AdminUserControls/AdminLogin.cs");
        //}

        //private void CloseOSK()
        //{
        //    LogHelper.Logger.Info("Starts CloseOSK Function in AdminUserControls/AdminLogin.cs");
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //    LogHelper.Logger.Info("Ends CloseOSK Function in AdminUserControls/AdminLogin.cs");
        //}

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {

                if (txtName.Text == "admin" && txtPassword.Password == "123")
                {
                    Apps.KillOSK();
                    EvntLoginAdmin(this, null);
                }
                else
                {
                    MessageBox.Show("Please Enter Valid UserName and Password....", "DRL", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtName.Text = string.Empty;
                    txtPassword.Password = string.Empty;
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                if (txtName.Text == "admin" && txtPassword.Password == "123")
                {
                    Apps.KillOSK();
                    EvntLoginAdmin(this, null);
                }
                else
                {
                    MessageBox.Show("Please Enter Valid UserName and Password....", "DRL", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtName.Text = string.Empty;
                    txtPassword.Password = string.Empty;
                }
            }
        }
        private void GrdMainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
    }
}
