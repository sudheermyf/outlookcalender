﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OutlookCalendar.AdminUserControls
{
    /// <summary>
    /// Interaction logic for RoomSettings.xaml
    /// </summary>
    public partial class RoomSettings : UserControl
    {
        public RoomSettings()
        {
            InitializeComponent();
        }
        Process proc;
        string processName;
        public event EventHandler EvntCloseRoomSettings;
        private Configuration config;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            EvntCloseRoomSettings(this, null);
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            txtRoomEmailID.Text = config.AppSettings.Settings["RoomSettings"].Value;
        }
        private void btnSave_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            }
            config.AppSettings.Settings["RoomSettings"].Value = txtRoomEmailID.Text; //save room email Id here
            config.Save(ConfigurationSaveMode.Full, true);
            MessageBox.Show("Saved Successfully", "DRL");
            EvntCloseRoomSettings(this, null);
        }
        private void OpenVirtualKeyboard()
        {
            try
            {
                proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
                processName = proc.ProcessName;
            }
            catch (Exception ex)
            {
            }
        }
        //private void CloseOSK()
        //{
        //    Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
        //    foreach (Process proc in process)
        //    {
        //        proc.CloseMainWindow();
        //    }
        //}

        private void txtRoomEmailID_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void txtRoomEmailID_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            //txtRoomEmailID.Focus();
        }

        private void GrdMainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }
    }
}
