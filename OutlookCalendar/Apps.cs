﻿using DRLPortalDB.DbFolder;
using Microsoft.Exchange.WebServices.Data;
using OutlookCalendar.Model;
using OutlookCalendar.UserControls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace OutlookCalendar
{
    public static class Apps
    {
        public static DRLPortalDBContext dbContext = new DRLPortalDBContext();
        public static SmartCardReaderValues smartCardReaderValues = new SmartCardReaderValues();
        public static string USERNAME { get; set; }
        public static string EMAIL { get; set; }
        public static ExchangeService service { get; set; }
        public static List<String> lstMeetingTime { get; set; }
        public static bool FromAutoLogin { get; set; }

        public static void PingTest(bool raiseWindow = true)
        {
            try
            {
                Ping ping = new Ping();

                PingReply pingStatus = ping.Send("www.google.com");

                if (pingStatus.Status == IPStatus.Success)
                {

                }
                else
                {
                    if (raiseWindow)
                    {
                        InternetDown downDlg = new InternetDown();
                        downDlg.ShowDialog();
                    }
                }
            }
            catch (Exception)
            {
                if (raiseWindow)
                {
                    InternetDown downDlg = new InternetDown();
                    downDlg.ShowDialog();
                }
            }
        }

        public static bool PingCheck()
        {
            try
            {
                Ping ping = new Ping();

                PingReply pingStatus = ping.Send("www.google.com");

                if (pingStatus.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void KillOSK()
        {
            try
            {
                if (Process.GetProcessesByName("TabTip").Count() > 0)
                {
                    Process fetchProcess = Process.GetProcessesByName("TabTip").FirstOrDefault();
                    if (fetchProcess != null)
                    {
                        fetchProcess.Kill();
                    }
                }
            }
            catch (Exception)
            { }
        }

        public static void OpenVirtualKeyboard()
        {
            try
            {
                Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
            }
            catch (Exception)
            {
            }
        }
    }
}
