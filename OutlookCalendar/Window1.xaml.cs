﻿using System;
using System.Collections.Generic;
using OutlookCalendar.UserControls.AlertMessageControls;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media; 
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OutlookCalendar.Model;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using OutlookCalendar.UserControls;
using OutlookCalendar.AdminUserControls;
using OutlookCalendar.UserControls.ScheduleControls;
using Office365Communication.Meetings;
using Microsoft.Exchange.WebServices.Data;
using System.Collections.ObjectModel;
using Office365Communication;
using WpfScheduler;
using System.ComponentModel;
using System.Configuration;
using System.Reflection;
using OutlookCalendar.Logic;
using Subsembly.SmartCard;
using System.Threading;

namespace OutlookCalendar
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        DispatcherTimer idleTimer;
        private Appointments appointments = new Appointments();
        Microsoft.Exchange.WebServices.Data.ExchangeService service;
        private int previousMonth = 0, idleTimerSeconds = 0;
        public bool InitialLoad = true;
        public string LoginSuccessMsg { get; set; }
        string Message;
        private Configuration config;
        public string LoadCompleteMsg { get; set; }
        public Calendar CalendarItem { get; set; }
        public InitiateSmartCard initiateSmartCard;
        private BackgroundWorker bgWorker;
        private DRLLogin login;
        public Window1()
        {
            InitializeComponent();
            DataContext = appointments;
        }
        private void btnBookRoomHome_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnBookRoomHome_PreviewTouchUp Function in Window1.cs");
            try
            {
                Apps.PingTest();
                LogHelper.Logger.Info("Starts Try block in btnBookRoomHome_PreviewTouchUp Function in Window1.cs");
                MonthlyCalendar.SelectedDate = DateTime.Now;
                if(bgWorker != null && !bgWorker.IsBusy)
                {
                    bgWorker.RunWorkerAsync(false);
                }
                LogHelper.Logger.Info("End Try block in btnBookRoomHome_PreviewTouchUp Function in Window1.cs");
            }
            catch (Exception)
            {
                LogHelper.Logger.Info("Ends Catch block in btnBookRoomHome_PreviewTouchUp Function in Window1.cs");
                Message = "Loading Failed.";
                LoadErrorMsg(Message);
            }
            LogHelper.Logger.Info("ends btnBookRoomHome_PreviewTouchUp Function in Window1.cs");
        }
        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnBack_PreviewTouchUp_1 Function in Window1.cs");
            Storyboard LoadHome = TryFindResource("LoadHome_SB") as Storyboard;
            LoadHome.Begin();
            ParentPanel.Children.Clear();
            grdCalendar.Visibility = Visibility.Hidden;
            grdHomePage.Visibility = Visibility.Visible;
            LoadHomePage();
            LoadHome.Completed -= LoadHome_Completed;
            LoadHome.Completed += LoadHome_Completed;
            LogHelper.Logger.Info("ends btnBack_PreviewTouchUp_1 Function in Window1.cs");
        }

        void LoadHome_Completed(object sender, EventArgs e)
        {
            Storyboard LoadHome = sender as Storyboard;
            if (LoadHome != null)
            {
                LoadHome.Remove();
            }
        }
        private void LoadHomePage()
        {
            LogHelper.Logger.Info("Starts LoadHomePage Function in Window1.cs");

            if (bgWorker != null && !bgWorker.IsBusy)
            {
                bgWorker.RunWorkerAsync(true);
            }

            LogHelper.Logger.Info("ends LoadHomePage Function in Window1.cs");
        }

        private void RemoveLoadingScreen()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in mainLayout.Children)
            {
                if (item is Loading)
                {
                    lstIndexes.Add(mainLayout.Children.IndexOf(item));
                }
            }

            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                mainLayout.Children.RemoveAt(itemIndex);
            }
        }

        private void BindTodaysMeetingList(List<Appointment> fetchedAppointments)
        {
            LogHelper.Logger.Info("Starts BindTodaysMeetingList Function in Window1.cs");

            DateTime dtNow = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
            DateTime dtEndToday = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");
            this.Dispatcher.Invoke((Action)(() =>
            {
                if (fetchedAppointments != null && fetchedAppointments.Count > 0 && TodayMeetingList != null)
                {
                    TodayMeetingList.ItemsSource = null;
                    TodayMeetingList.ItemsSource = fetchedAppointments.Where(c => c.Start >= dtNow && c.Start <= dtEndToday).ToList();
                }
                else
                {
                    TodayMeetingList.ItemsSource = null;
                    TodayMeetingList.ItemsSource = new List<Appointment>();
                }

                lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
                lblDayMonthDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");
            }));
            LogHelper.Logger.Info("Ends BindTodaysMeetingList Function in Window1.cs");
        }

        private Collection<Appointment> GetEventsByRoom(DateTime dtNow, DateTime dtEndToday)
        {
            LogHelper.Logger.Info("Starts GetEventsByRoom Function in Window1.cs");
            Collection<Appointment> appointment = new Collection<Appointment>();
            try
            {
                LogHelper.Logger.Info("Starts Try block in GetEventsByRoom Function in Window1.cs");
                string room = Convert.ToString(config.AppSettings.Settings["RoomSettings"].Value);
                if (!string.IsNullOrEmpty(room))
                {
                    IEnumerable<CalendarEvent> lstCalendarEvents = appointments.GetRoomEvents(Apps.service, room, dtNow, dtEndToday);
                    if (lstCalendarEvents != null)
                    {
                        lstCalendarEvents.ToList().ForEach(c =>
                        {
                            Appointment newAppointment = new Appointment(Apps.service);
                            newAppointment.Subject = "Reserved";
                            newAppointment.Start = c.StartTime;
                            newAppointment.End = c.EndTime;
                            appointment.Add(newAppointment);
                        });
                    }
                }
                LogHelper.Logger.Info("Ends Try block in GetEventsByRoom Function in Window1.cs");
            }
            catch (Exception)
            {
                LogHelper.Logger.Info("Starts Catch block in GetEventsByRoom Function in Window1.cs");
            }
            
            LogHelper.Logger.Info("Ends GetEventsByRoom Function in Window1.cs");
            return appointment;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts Window_Loaded_1 Function in Window1.cs");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            string room = Convert.ToString(config.AppSettings.Settings["RoomSettings"].Value);
            txtHomePageRoomName.Text = room;
            lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
            lblDayMonthDate.Text = DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.Date.ToString("dd");
            SlideShow slideshow = new SlideShow();
            ParentPanel.Children.Add(slideshow);
            slideshow.EvntCloseSlideShow += slideshow_EvntCloseSlideShow;
            AutoAuthenticate();
            InitiateSmartCard();

            idleTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
            idleTimer.Interval = new TimeSpan(0, 0, 5);
            idleTimer.Tick += idleTimer_Tick;
            //idleTimer.Start();
            if (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds) && idleTimerSeconds <= 120))
            {
                idleTimerSeconds = 120;
            }

            if (bgWorker == null)
            {
                bgWorker = new BackgroundWorker();
                bgWorker.WorkerReportsProgress = true;
                bgWorker.WorkerSupportsCancellation = true;
                bgWorker.DoWork += bgWorker_DoWork;
                bgWorker.ProgressChanged += bgWorker_ProgressChanged;
                bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            }

            LogHelper.Logger.Info("ends Window_Loaded_1 Function in Window1.cs");
        }

        private void InitiateSmartCard()
        {
            try
            {
                if (!CardTerminalManager.Singleton.StartedUp)
                {
                    initiateSmartCard = new InitiateSmartCard();
                    initiateSmartCard.refreshValues += initiateSmartCard_refreshValues;

                    new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;
                        initiateSmartCard.DetechSmartCardReader();
                    }).Start();

                }
            }
            catch (Exception ex)
            {
                //log here
            }
        }

        void initiateSmartCard_refreshValues(object sender, EventArgs e)
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    if (login != null)
                    {
                        login.DataContext = Apps.smartCardReaderValues;
                        login.txtRFID.Password = Apps.smartCardReaderValues.Rfid;
                        if (!string.IsNullOrEmpty(Apps.smartCardReaderValues.Rfid))
                        {
                            login.InitiateAuthentication();
                        }
                    }
                }));
            }
            catch (Exception)
            {

            }
        }


        void meetingCheckTimer_Tick(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts meetingCheckTimer_Tick Function in Window1.cs");

            if (!string.IsNullOrEmpty(Apps.USERNAME))
            {
                txtRoomName.Text = Apps.USERNAME;
                brdUserCred.Visibility = Visibility.Visible;
            }

            if (TodayMeetingList != null && TodayMeetingList.ItemsSource != null)
            {
                Collection<Appointment> lstTodayAppointments = (Collection<Appointment>)TodayMeetingList.ItemsSource;
                if (lstTodayAppointments != null && lstTodayAppointments.Count > 0)
                {
                    if(lstTodayAppointments.ToList().Exists(c => c.Start >= DateTime.Now && c.End <= DateTime.Now))
                    {
                        greenBg.Visibility = Visibility.Collapsed;
                        orangeBg.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        greenBg.Visibility = Visibility.Visible;
                        orangeBg.Visibility = Visibility.Collapsed;
                    }
                }
            }

            LogHelper.Logger.Info("Ends meetingCheckTimer_Tick Function in Window1.cs");
        }

        private void AutoAuthenticate()
        {
            LogHelper.Logger.Info("Starts AutoAuthenticate Function in Window1.cs");
            BackgroundWorker bgWorkerLogin = new BackgroundWorker();
            bgWorkerLogin.WorkerSupportsCancellation = true;
            bgWorkerLogin.WorkerReportsProgress = true;
            bgWorkerLogin.DoWork += bgWorkerLogin_DoWork;
            bgWorkerLogin.RunWorkerCompleted += bgWorkerLogin_RunWorkerCompleted;
            bgWorkerLogin.RunWorkerAsync();
            LogHelper.Logger.Info("Ends AutoAuthenticate Function in Window1.cs");
        }

        void bgWorkerLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LogHelper.Logger.Info("Auto Login bgWorkerLogin_RunWorkerCompleted");
            txtRoomName.Text = string.Empty;
            brdUserCred.Visibility = Visibility.Collapsed;
            RemoveLoadingScreen();
            if (LoginSuccessMsg.Equals("Success"))
            {
                Apps.FromAutoLogin = true;
            }
            LogHelper.Logger.Info("Ends bgWorker_RunWorkerCompleted Function in UserControls/Login.cs");
        }

        void bgWorkerLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorkerLogin_DoWork Function in Window1.cs");
            string userName = Convert.ToString(config.AppSettings.Settings["AutoLoginUserName"].Value);
            string password = Convert.ToString(config.AppSettings.Settings["AutoLoginPassword"].Value);
            LoginSuccessMsg = "Success";

            try
            {
                ShowLoadingScreen();
                if(InitialLoad)
                {
                    Apps.dbContext = new DRLPortalDB.DbFolder.DRLPortalDBContext();
                    Apps.dbContext.ADUSERINFO.ToList();
                    InitialLoad = false;
                }
                ExchangeService service = Authentication.AuthenticateService(userName, password);
                Apps.service = service;
            }
            catch (Exception ex)
            {
                LoginSuccessMsg = ex.Message;
                LogHelper.Logger.Info("Error : " + ex.Message + " bgWorkerLogin_DoWork Function");
            }
            LogHelper.Logger.Info("Ends bgWorkerLogin_DoWork Function.");
        }

        private void ShowLoadingScreen()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                btnAdminLogin.Visibility = Visibility.Visible;
                Apps.PingTest();
                Loading loading = new Loading();
                Panel.SetZIndex(loading, 9999);
                loading.BringIntoView();
                mainLayout.Children.Add(loading);
            }));
        }

        void idleTimer_Tick(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts idleTimer_Tick Function in Window1.cs");
            lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
            TimeSpan span = IdleTimeSetter.GetLastInput();
            if (idleTimerSeconds <= (Convert.ToInt32(span.TotalSeconds)))
            {
                ParentPanel.Children.Clear();
                GrdSettingsPanel.Children.Clear();
                SlideShow slideshow = new SlideShow();
                slideshow.EvntCloseSlideShow += slideshow_EvntCloseSlideShow;
                ParentPanel.Children.Add(slideshow);
                AutoAuthenticate();
                txtRoomName.Text = string.Empty;
                Apps.USERNAME = string.Empty;
                login = null;
                brdUserCred.Visibility = Visibility.Collapsed;
                //Slide Show start
                idleTimer.Stop();
            }
            LogHelper.Logger.Info("Ends checking idleTimer_Tick Function in Window1.cs");

            LogHelper.Logger.Info("Starts Meeting color changer idleTimer_Tick Function in Window1.cs");
            if (!string.IsNullOrEmpty(Apps.USERNAME))
            {
                txtRoomName.Text = Apps.USERNAME;
                brdUserCred.Visibility = Visibility.Visible;
            }

            if (TodayMeetingList != null && TodayMeetingList.ItemsSource != null)
            {
                List<Appointment> lstTodayAppointments = TodayMeetingList.ItemsSource as List<Appointment>;
                if (lstTodayAppointments != null && lstTodayAppointments.Count > 0)
                {
                    if (lstTodayAppointments.ToList().Exists(c => c.Start <= DateTime.Now && c.End >= DateTime.Now))
                    {
                        greenBg.Visibility = Visibility.Collapsed;
                        orangeBg.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        greenBg.Visibility = Visibility.Visible;
                        orangeBg.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    greenBg.Visibility = Visibility.Visible;
                    orangeBg.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                greenBg.Visibility = Visibility.Visible;
                orangeBg.Visibility = Visibility.Collapsed;
            }
            LogHelper.Logger.Info("Ends Meeting color changer idleTimer_Tick Function in Window1.cs");
        }
        void slideshow_EvntCloseSlideShow(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts slideshow_EvntCloseSlideShow Function in Window1.cs");
            //remove slideshow from ParentPanel Layout.
            SlideShow slideshow = (SlideShow)sender;
            ParentPanel.Children.Remove(slideshow);
            idleTimer.Start();
            //Add login page
            if (!Apps.FromAutoLogin)
            {
                login = new DRLLogin();
                login.EvntErrorAlert += login_EvntErrorAlert;
                login.ShowDialog();
                txtRoomName.Text = Apps.USERNAME;
                brdUserCred.Visibility = Visibility.Visible;
                //raise events from Login.xaml
            }
            Storyboard LoadHome = TryFindResource("LoadHome_SB") as Storyboard;
            LoadHome.Begin();
            grdCalendar.Visibility = Visibility.Hidden;
            grdHomePage.Visibility = Visibility.Visible;
            LoadHomePage();
            LogHelper.Logger.Info("Ends slideshow_EvntCloseSlideShow Function in Window1.cs");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_DoWork Function in Window1.cs");
            LoadCompleteMsg = string.Empty;
            try
            {
                ShowLoadingScreen();

                this.Dispatcher.Invoke((Action)(() =>
                {
                    scheduler1.SelectedDate = DateTime.Now;
                    previousMonth = scheduler1.SelectedDate.Month;
                }));

                List<Appointment> lstFetchedAppointments = RefreshAppointments(DateTime.Now, DateTime.Now);

                RefreshAndBindData(lstFetchedAppointments = null);
                LoadCompleteMsg = "Success";
                e.Result = e.Argument;
            }
            catch (Exception ex)
            {
                LoadCompleteMsg = ex.Message;
                bgWorker.ReportProgress(1, ex);
            }
            LogHelper.Logger.Info("Ends bgWorker_DoWork Function in Window1.cs");
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is Exception)
            {
                LoadErrorMsg(((Exception)e.UserState).Message);
            }
            else if (e.UserState is string)
            {
                LoadSuccessMsg((string)e.UserState);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LogHelper.Logger.Info("Starts bgWorker_RunWorkerCompleted Function in Window1.cs");
            if (LoadCompleteMsg.Equals("Success"))
            {
                if (!string.IsNullOrEmpty(Apps.USERNAME))
                {
                    txtRoomName.Text = Apps.USERNAME;
                    brdUserCred.Visibility = Visibility.Visible;
                }

                bool hideCalendar = false;
                if (e.Result != null && e.Result is bool)
                {
                    hideCalendar = (bool)e.Result;
                }

                if (!hideCalendar)
                {
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        Storyboard LoadCalendar = TryFindResource("LoadCalendar_SB") as Storyboard;
                        LoadCalendar.Begin();
                        ParentPanel.Children.Clear();
                        grdCalendar.Visibility = Visibility.Visible;
                        grdHomePage.Visibility = Visibility.Hidden;
                    }));
                }
            }

            RemoveLoadingScreen();

            LogHelper.Logger.Info("Ends bgWorker_RunWorkerCompleted Function in Window1.cs");
        }

        private void RefreshAndBindData(List<Appointment> fetchedAppointments)
        {
            LogHelper.Logger.Info("Starts RefreshAndBindData Function in Window1.cs");

            DateTime startDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
            DateTime endDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");

            if (fetchedAppointments == null)
            {
                fetchedAppointments = RefreshAppointmentsByDate(startDate, endDate);
            }
            else if (fetchedAppointments.Count > 0)
            {
                fetchedAppointments = fetchedAppointments.Where(c => c.Start >= startDate && c.End <= endDate).ToList();
            }
            
            this.Dispatcher.Invoke((Action)(() =>
            {
                if (fetchedAppointments != null)
                {
                    ScheduledMeeting.ScheduledMeeting.ItemsSource = null;
                    ScheduledMeeting.ScheduledMeeting.ItemsSource = fetchedAppointments;
                    BindTodaysMeetingList(fetchedAppointments);
                }
            }));
            LogHelper.Logger.Info("Ends RefreshAndBindData Function in Window1.cs");
        }
        private void RefreshData()
        {
            LogHelper.Logger.Info("Starts RefreshData Function in Window1.cs");
            DateTime startDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
            DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            //DateTime endDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");
            List<Appointment> fetchedAppointments = RefreshAppointmentsByDate(startDate, endDate);
            if (fetchedAppointments != null && fetchedAppointments.Count > 0)
            {
                scheduler1.Events.Clear();
                fetchedAppointments.ToList().ForEach(c =>
                {
                    scheduler1.AddEvent(new Event() { Id = c.Id, Appointment = c, Subject = c.Subject, Start = c.Start, End = c.End, Color = Brushes.Orange });
                });
                RefreshAndBindData(fetchedAppointments);
            }
            LogHelper.Logger.Info("Ends RefreshData Function in Window1.cs");
        }
        private List<Appointment> RefreshAppointments(DateTime startDate, DateTime endDate)
        {
            LogHelper.Logger.Info("Starts RefreshAppointments Function in Window1.cs");

            Collection<Appointment> fetchedAppointments = new Collection<Appointment>();
            try
            {
                LogHelper.Logger.Info("Starts Try block in RefreshAppointments Function in Window1.cs");
                service = Apps.service;
                var firstDayOfMonth = new DateTime(startDate.Year, startDate.Month, 1);    //gets the first day of current month
                var lastDayOfMonth = new DateTime(endDate.Year, endDate.Month, DateTime.DaysInMonth(endDate.Year, endDate.Month)); //gets the last day of current month
                string room = Convert.ToString(config.AppSettings.Settings["RoomSettings"].Value);
                if (true)//Apps.FromAutoLogin)
                {
                    fetchedAppointments = GetEventsByRoom(firstDayOfMonth, lastDayOfMonth);
                }
                //else
                //{
                //    fetchedAppointments = appointments.GetAppointments(service, firstDayOfMonth, lastDayOfMonth);   //fetches appointments of current month
                //}

                this.Dispatcher.Invoke((Action)(() =>
                {
                    scheduler1.Events.Clear();
                    fetchedAppointments.ToList().ForEach(c =>
                    {
                        scheduler1.AddEvent(new Event() { Id = c.Id, Appointment = c, Subject = c.Subject, Start = c.Start, End = c.End, Color = Brushes.Orange });
                        if (Apps.lstMeetingTime == null)
                            Apps.lstMeetingTime = new List<string>();
                        Apps.lstMeetingTime.Add(c.Start.ToString() + "-" + c.End.ToString());
                    });
                }));

                LogHelper.Logger.Info("Ends Try block in RefreshAppointments Function in Window1.cs");
                LogHelper.Logger.Info("Ends RefreshAppointments Function in Window1.cs");
            }
            catch (Exception)
            {
                LogHelper.Logger.Info("Starts Catch block in RefreshAppointments Function in Window1.cs");
            }
            return fetchedAppointments.ToList();
        }
        private List<Appointment> RefreshAppointmentsByDate(DateTime startDate, DateTime endDate)
        {
            LogHelper.Logger.Info("Starts RefreshAppointmentsByDate Function in Window1.cs");
            Collection<Appointment> fetchedAppointments = new Collection<Appointment>();
            if (true)//Apps.FromAutoLogin)
            {
                fetchedAppointments = GetEventsByRoom(startDate, endDate);
                this.Dispatcher.Invoke((Action)(() =>
                {
                    scheduler1.Events.Clear();
                    fetchedAppointments.ToList().ForEach(c =>
                    {
                        scheduler1.AddEvent(new Event() { Id = c.Id, Appointment = c, Subject = c.Subject, Start = c.Start, End = c.End, Color = Brushes.Orange });
                    });
                }));
            }
            //else
            //{
            //    fetchedAppointments = appointments.GetAppointments(Apps.service, startDate, endDate);
            //}
            LogHelper.Logger.Info("Ends RefreshAppointmentsByDate Function in Window1.cs");
            return fetchedAppointments.ToList();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts timer_Tick Function in Window1.cs");
            LoadHomePage();
            LogHelper.Logger.Info("Ends timer_Tick Function in Window1.cs");
        }
        void login_EvntErrorAlert(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts login_EvntErrorAlert Function in Window1.cs");
            LoadErrorMsg(sender);   //show error alert where sender is a message
            LogHelper.Logger.Info("Ends login_EvntErrorAlert Function in Window1.cs");
        }
        #region AlertMessages
        private void LoadErrorMsg(object Message)
        {
            MessageBox.Show(Message.ToString());
            //ErrorAlert alert = new ErrorAlert();
            //alert.txtAlertMessage.Text = Message.ToString();
            //GrdMessageDisplayPanel.Children.Add(alert);
            ////raise events from ErrorAlert page
            //alert.EvntCloseErrorAlert += alert_EvntCloseErrorAlert; //this event raises automatically when Storyboard completes
        }
        void alert_EvntCloseErrorAlert(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts alert_EvntCloseErrorAlert Function in Window1.cs");
            //close errorAlert automatically
            ErrorAlert alert = (ErrorAlert)sender;
            if (alert != null)
            {
                if (alert.delayTimer != null)
                {
                    alert.delayTimer.Stop();
                }
                GrdMessageDisplayPanel.Children.Remove(alert);
            }
            LogHelper.Logger.Info("Ends alert_EvntCloseErrorAlert Function in Window1.cs");
        }
        private void LoadSuccessMsg(object Message)
        {
            MessageBox.Show(Message.ToString());
            //SuccessAlert sAlert = new SuccessAlert();
            //sAlert.txtAlertMessage.Text = Message.ToString();
            //GrdMessageDisplayPanel.Children.Add(sAlert);
            ////raise events from SuccessAlert.xaml
            //sAlert.EvntCloseSuccessAlert -= sAlert_EvntCloseSuccessAlert;
            //sAlert.EvntCloseSuccessAlert += sAlert_EvntCloseSuccessAlert;
        }
        void sAlert_EvntCloseSuccessAlert(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts sAlert_EvntCloseSuccessAlert Function in Window1.cs");
            //close SuccessAlert automatically
            SuccessAlert sAlert = (SuccessAlert)sender;
            if (sAlert != null)
            {
                if (sAlert.delayTimer != null)
                {
                    sAlert.delayTimer.Stop();
                }
                GrdMessageDisplayPanel.Children.Remove(sAlert);
            }
            LogHelper.Logger.Info("Ends sAlert_EvntCloseSuccessAlert Function in Window1.cs");
        }
        #endregion

        #region CalenderEvents
        private void scheduler1_Loaded(object sender, RoutedEventArgs e)
        {
            LogHelper.Logger.Info("Starts scheduler1_Loaded Function in Window1.cs");
            try
            {
                scheduler1.SelectedDate = DateTime.Now;
                scheduler1.Mode = Mode.Day;
                scheduler1.StartJourney = new TimeSpan(0, 0, 0);
                scheduler1.EndJourney = new TimeSpan(24, 0, 0);
                scheduler1.OnEventDoubleClick -= scheduler1_OnEventDoubleClick;
                scheduler1.OnScheduleDoubleClick -= scheduler1_OnScheduleDoubleClick;
                scheduler1.Loaded -= scheduler1_Loaded;

                scheduler1.OnEventDoubleClick += scheduler1_OnEventDoubleClick;
                scheduler1.OnScheduleDoubleClick += scheduler1_OnScheduleDoubleClick;
                scheduler1.Loaded += scheduler1_Loaded;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            LogHelper.Logger.Info("Ends scheduler1_Loaded Function in Window1.cs");
        }
        void scheduler1_OnScheduleDoubleClick(object sender, DateTime e)
        {
            LogHelper.Logger.Info("Starts scheduler1_OnScheduleDoubleClick Function in Window1.cs");
            Schedule mainSchedule = ParentPanel.Children.OfType<Schedule>().FirstOrDefault();
            if (mainSchedule == null)
            {
                if (!string.IsNullOrEmpty(Apps.USERNAME))
                {
                    txtRoomName.Text = Apps.USERNAME;
                    brdUserCred.Visibility = Visibility.Visible;
                }
                if (Apps.FromAutoLogin)
                {
                    //check login before scheduling appointment
                    login = new DRLLogin();
                    login.EvntErrorAlert += login_EvntErrorAlert;
                    login.ShowDialog();
                    if (!Apps.FromAutoLogin)
                    {
                        Schedule sch = new Schedule();
                        sch.SelectedDate = e;
                        ParentPanel.Children.Add(sch);
                        //raise events from Schedule.xaml
                        sch.EvntScheduleToCalendar += sch_EvntScheduleToCalendar;
                        sch.EvntSchSuccessMsg += sch_EvntSchSuccessMsg;
                        sch.EvntSchErrorMsg += sch_EvntSchErrorMsg;
                        sch.EvntUserLogout += sch_EvntUserLogout;
                        //btnBack_PreviewTouchUp_1(sender, null);
                    }
                }
                else
                {
                    Schedule sch = new Schedule();
                    sch.SelectedDate = e;
                    ParentPanel.Children.Add(sch);
                    //raise events from Schedule.xaml
                    sch.EvntScheduleToCalendar += sch_EvntScheduleToCalendar;
                    sch.EvntSchSuccessMsg += sch_EvntSchSuccessMsg;
                    sch.EvntSchErrorMsg += sch_EvntSchErrorMsg;
                    sch.EvntUserLogout += sch_EvntUserLogout;
                }
            }
            LogHelper.Logger.Info("Ends scheduler1_OnScheduleDoubleClick Function in Window1.cs");
        }

        void sch_EvntUserLogout(object sender, EventArgs e)
        {
            UserLogout(sender);
        }
        void scheduler1_OnEventDoubleClick(object sender, Event e)
        {
            LogHelper.Logger.Info("Starts scheduler1_OnEventDoubleClick Function in Window1.cs");
            Console.WriteLine(e.Subject);
            LogHelper.Logger.Info("Ends scheduler1_OnEventDoubleClick Function in Window1.cs");
        }

        private void scheduler1_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts scheduler1_TouchDown Function in Window1.cs");
            Schedule sch = new Schedule();
            ParentPanel.Children.Add(sch);
            //raise events from Schedule.xaml
            sch.EvntScheduleToCalendar += sch_EvntScheduleToCalendar;
            sch.EvntSchSuccessMsg += sch_EvntSchSuccessMsg;
            sch.EvntSchErrorMsg += sch_EvntSchErrorMsg;
            sch.EvntUserLogout += sch_EvntUserLogout;
            LogHelper.Logger.Info("Ends scheduler1_TouchDown Function in Window1.cs");
        }
        void sch_EvntSchSuccessMsg(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts sch_EvntSchSuccessMsg Function in Window1.cs");
            LoadSuccessMsg(sender); //load success alert with sender as message
            LogHelper.Logger.Info("Ends sch_EvntSchSuccessMsg Function in Window1.cs");
        }
        void sch_EvntSchErrorMsg(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts sch_EvntSchErrorMsg Function in Window1.cs");
            LoadErrorMsg(sender);   //load error alert with sender as message
            LogHelper.Logger.Info("Ends sch_EvntSchErrorMsg Function in Window1.cs");
        }
        void sch_EvntScheduleToCalendar(object sender, EventArgs e)
        {
            Apps.PingTest();
            LogHelper.Logger.Info("Starts sch_EvntScheduleToCalendar Function in Window1.cs");
            Schedule sch = (Schedule)sender;
            ParentPanel.Children.Remove(sch);
            if (!string.IsNullOrEmpty(Apps.USERNAME))
            {
                txtRoomName.Text = Apps.USERNAME;
                brdUserCred.Visibility = Visibility.Visible;
            }
            RefreshData();
            LogHelper.Logger.Info("Ends sch_EvntScheduleToCalendar Function in Window1.cs");
        }
        #endregion

        private void btnDay_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnDay_PreviewTouchUp Function in Window1.cs");
            scheduler1.Mode = Mode.Day;
            LogHelper.Logger.Info("Ends btnDay_PreviewTouchUp Function in Window1.cs");
        }

        private void btnMonth_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnMonth_PreviewTouchUp Function in Window1.cs");
            scheduler1.Mode = Mode.Month;
            LogHelper.Logger.Info("Ends btnMonth_PreviewTouchUp Function in Window1.cs");
        }

        private void btnWeek_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnWeek_PreviewTouchUp Function in Window1.cs");
            scheduler1.Mode = Mode.Week;
            LogHelper.Logger.Info("Ends btnWeek_PreviewTouchUp Function in Window1.cs");
        }

        private void btnPrev_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnPrev_TouchDown Function in Window1.cs");
            switch (scheduler1.Mode)
            {
                case Mode.Day:
                    FetchCalendarAppointments(false);
                    break;
                case Mode.Week:
                    FetchCalendarAppointments(false);
                    break;
                case Mode.Month:
                    FetchCalendarAppointments(false);
                    break;
                default:
                    break;
            }
            LogHelper.Logger.Info("Ends btnPrev_TouchDown Function in Window1.cs");
        }

        private void FetchCalendarAppointments(bool fromNext)
        {
            LogHelper.Logger.Info("Starts FetchCalendarAppointments Function in Window1.cs");
            int days = 0;
            int months = 1;
            if (scheduler1.Mode == Mode.Day)
            {
                if (!fromNext)
                {
                    days = -1;
                }
                else
                {
                    days = 1;
                }

                scheduler1.SelectedDate = scheduler1.SelectedDate.AddDays(days);
                if (!scheduler1.SelectedDate.Month.Equals(previousMonth))
                {
                    previousMonth = scheduler1.SelectedDate.Month;
                    RefreshAppointments(scheduler1.SelectedDate, scheduler1.SelectedDate);
                }
            }
            else if (scheduler1.Mode == Mode.Week)
            {
                if (!fromNext)
                {
                    days = -7;
                }
                else
                {
                    days = 7;
                }
                scheduler1.SelectedDate = scheduler1.SelectedDate.AddDays(days);
                if (!scheduler1.SelectedDate.Month.Equals(previousMonth))
                {
                    previousMonth = scheduler1.SelectedDate.Month;
                    RefreshAppointments(scheduler1.SelectedDate, scheduler1.SelectedDate);
                }
            }
            else if (scheduler1.Mode == Mode.Month)
            {
                if (!fromNext)
                {
                    months = -1;
                }
                else
                {
                    months = 1;
                }
                scheduler1.SelectedDate = scheduler1.SelectedDate.AddMonths(months);
                if (!scheduler1.SelectedDate.Month.Equals(previousMonth))
                {
                    previousMonth = scheduler1.SelectedDate.Month;
                    RefreshAppointments(scheduler1.SelectedDate, scheduler1.SelectedDate);
                }
            }
            LogHelper.Logger.Info("Ends FetchCalendarAppointments Function in Window1.cs");
        }

        private void btnNext_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnNext_TouchDown Function in Window1.cs");
            switch (scheduler1.Mode)
            {
                case Mode.Day:
                    FetchCalendarAppointments(true);
                    break;
                case Mode.Week:
                    FetchCalendarAppointments(true);
                    break;
                case Mode.Month:
                    FetchCalendarAppointments(true);
                    break;
                default:
                    break;
            }
            LogHelper.Logger.Info("Ends btnNext_TouchDown Function in Window1.cs");
        }


        void appPopUp_EvntClosePopUp(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts appPopUp_EvntClosePopUp Function in Window1.cs");
            List<int> lstIndexes = new List<int>(); //list to store Loading index value in ParentPanel
            foreach (UIElement item in ParentPanel.Children)
            {
                if (item is AppointmentPopUp || item is QuickViewAppointments)
                {
                    lstIndexes.Add(ParentPanel.Children.IndexOf(item));  //add Loading index value to lstIndexes
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            foreach (int itemIndex in lstIndexes)
            {
                ParentPanel.Children.RemoveAt(itemIndex);    //remove Loading by itemIndex                    
            }
            LogHelper.Logger.Info("Ends appPopUp_EvntClosePopUp Function in Window1.cs");
        }

        //private void btnBookRoomFooter_PreviewTouchUp(object sender, TouchEventArgs e)
        //{
        //    Apps.PingTest();
        //    btnBookRoomFooter.IsEnabled = false;
        //    LogHelper.Logger.Info("Starts btnBookRoomFooter_PreviewTouchUp Function in Window1.cs");
        //    Schedule mainSchedule = ParentPanel.Children.OfType<Schedule>().FirstOrDefault();
        //    if (mainSchedule == null)
        //    {
        //        if (Apps.FromAutoLogin)
        //        {
        //            //check login before scheduling appointment
        //            login = new DRLLogin();
        //            login.EvntErrorAlert += login_EvntErrorAlert;
        //            login.ShowDialog();
        //            if (!Apps.FromAutoLogin)
        //            {
        //                Schedule sch = new Schedule();
        //                ParentPanel.Children.Add(sch);
        //                //raise events from Schedule.xaml
        //                sch.EvntScheduleToCalendar += sch_EvntScheduleToCalendar;
        //                sch.EvntSchSuccessMsg += sch_EvntSchSuccessMsg;
        //                sch.EvntSchErrorMsg += sch_EvntSchErrorMsg;
        //                sch.EvntUserLogout += sch_EvntUserLogout;
        //                //btnBack_PreviewTouchUp_1(sender, null);
        //            }
        //        }
        //        else
        //        {
        //            Schedule sch = new Schedule();
        //            ParentPanel.Children.Add(sch);
        //            //raise events from Schedule.xaml
        //            sch.EvntScheduleToCalendar += sch_EvntScheduleToCalendar;
        //            sch.EvntSchSuccessMsg += sch_EvntSchSuccessMsg;
        //            sch.EvntSchErrorMsg += sch_EvntSchErrorMsg;
        //            sch.EvntUserLogout += sch_EvntUserLogout;
        //        }
        //    }
        //    btnBookRoomFooter.IsEnabled = true;
        //    LogHelper.Logger.Info("Ends btnBookRoomFooter_PreviewTouchUp Function in Window1.cs");
        //}

        private void MonthlyCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts MonthlyCalendar_SelectedDatesChanged Function in Window1.cs");
                CalendarItem = (Calendar)sender;
                if (bgWorker != null && !bgWorker.IsBusy)
                {
                    bgWorker.RunWorkerAsync(false);
                }
            }
            catch (Exception)
            {
                LogHelper.Logger.Info("starts Catch in Window1.cs");
                Message = "Loading Failed.";
                LoadErrorMsg(Message);
                LogHelper.Logger.Info("ends Catch in Window1.cs");
            }
            LogHelper.Logger.Info("Ends MonthlyCalendar_SelectedDatesChanged Function in Window1.cs");
        }

        #region AdminSettings
        //GrdSettingsPanel is the main layout panel for admin settings.
        //adding of admin usercontrols is done only in GrdSettingsPanel

        private void btnAdminLogin_TouchDown(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts btnAdminLogin_TouchDown Function in Window1.cs");
            btnAdminLogin.Visibility = Visibility.Collapsed;
            AdminLogin admLogin = new AdminLogin();
            GrdSettingsPanel.Children.Add(admLogin);
            //Raise events from admin login page
            admLogin.EvntLoginAdmin += admLogin_EvntLoginAdmin;
            admLogin.EvntCloseAdminLogin += admLogin_EvntCloseAdminLogin;
            LogHelper.Logger.Info("Ends btnAdminLogin_TouchDown Function in Window1.cs");
        }
        void admLogin_EvntLoginAdmin(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admLogin_EvntLoginAdmin Function in Window1.cs");
            LoadAdminSettings();
            LogHelper.Logger.Info("Ends admLogin_EvntLoginAdmin Function in Window1.cs");
        }

        private void LoadAdminSettings()
        {
            LogHelper.Logger.Info("Starts LoadAdminSettings Function in Window1.cs");
            btnAdminLogin.Visibility = Visibility.Collapsed;
            GrdSettingsPanel.Children.Clear();
            AdminSettings admSettings = new AdminSettings();
            GrdSettingsPanel.Children.Add(admSettings);
            //Raise events from AdminSettings page
            admSettings.EvntLogoutSettings += admSettings_EvntLogoutSettings;
            admSettings.EvntSlideShowImages += admSettings_EvntSlideShowImages;
            admSettings.EvntSlideShowIdleTimer += admSettings_EvntSlideShowIdleTimer;
            admSettings.EvntOpenAutoLogin += admSettings_EvntOpenAutoLogin;
            admSettings.EvntOpenRoomSettings += admSettings_EvntOpenRoomSettings;
            LogHelper.Logger.Info("Ends LoadAdminSettings Function in Window1.cs");
        }

        void admSettings_EvntOpenRoomSettings(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admSettings_EvntOpenRoomSettings Function in Window1.cs");
            RoomSettings rmSetting = new RoomSettings();
            GrdSettingsPanel.Children.Clear();
            GrdSettingsPanel.Children.Add(rmSetting);
            //raise events form room settings page
            rmSetting.EvntCloseRoomSettings += rmSetting_EvntCloseRoomSettings;
            LogHelper.Logger.Info("Ends admSettings_EvntOpenRoomSettings Function in Window1.cs");
        }

        void rmSetting_EvntCloseRoomSettings(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts rmSetting_EvntCloseRoomSettings Function in Window1.cs");
            RoomSettings rmSettings = (RoomSettings)sender;
            GrdSettingsPanel.Children.Remove(rmSettings);
            LoadAdminSettings();
            LogHelper.Logger.Info("Ends rmSetting_EvntCloseRoomSettings Function in Window1.cs");
        }

        void admSettings_EvntOpenAutoLogin(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admSettings_EvntOpenAutoLogin Function in Window1.cs");
            GrdSettingsPanel.Children.Clear();
            AutoLogin autoLogin = new AutoLogin();
            GrdSettingsPanel.Children.Add(autoLogin);
            //Raise events from auto login page
            autoLogin.EvntCloseAutoLogin += autoLogin_EvntCloseAutoLogin;
            LogHelper.Logger.Info("Ends admSettings_EvntOpenAutoLogin Function in Window1.cs");
        }

        void autoLogin_EvntCloseAutoLogin(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts autoLogin_EvntCloseAutoLogin Function in Window1.cs");
            AutoLogin autoLogin = (AutoLogin)sender;
            GrdSettingsPanel.Children.Remove(autoLogin);
            LoadAdminSettings();
            LogHelper.Logger.Info("Ends autoLogin_EvntCloseAutoLogin Function in Window1.cs");
        }
        void admSettings_EvntSlideShowIdleTimer(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admSettings_EvntSlideShowIdleTimer Function in Window1.cs");
            //Code to Slideshow Idle Timer
            GrdSettingsPanel.Children.Clear();
            SlideShowIdleTimer ssIdleTimer = new SlideShowIdleTimer();
            GrdSettingsPanel.Children.Add(ssIdleTimer);
            //Raise events from SlideShowIdleTimer control
            ssIdleTimer.EvntCloseIdleTimer += ssIdleTimer_EvntCloseIdleTimer;
            LogHelper.Logger.Info("Ends admSettings_EvntSlideShowIdleTimer Function in Window1.cs");
        }

        void ssIdleTimer_EvntCloseIdleTimer(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts ssIdleTimer_EvntCloseIdleTimer Function in Window1.cs");
            SlideShowIdleTimer ssIdleTimer = new SlideShowIdleTimer();
            GrdSettingsPanel.Children.Remove(ssIdleTimer);
            LoadAdminSettings();
            LogHelper.Logger.Info("Ends ssIdleTimer_EvntCloseIdleTimer Function in Window1.cs");
        }
        void admSettings_EvntSlideShowImages(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admSettings_EvntSlideShowImages Function in Window1.cs");
            //Code to slideshow images adding
            GrdSettingsPanel.Children.Clear();
            SlideShowImages ssImages = new SlideShowImages();
            GrdSettingsPanel.Children.Add(ssImages);
            //Raise events from SlideShowImages control
            ssImages.EvntCloseSlideShow += ssImages_EvntCloseSlideShow;
            LogHelper.Logger.Info("Ends admSettings_EvntSlideShowImages Function in Window1.cs");
        }

        void ssImages_EvntCloseSlideShow(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts ssImages_EvntCloseSlideShow Function in Window1.cs");
            SlideShowImages ssImages = (SlideShowImages)sender;
            GrdSettingsPanel.Children.Remove(ssImages);
            LoadAdminSettings();
            LogHelper.Logger.Info("Ends ssImages_EvntCloseSlideShow Function in Window1.cs");
        }
        void admSettings_EvntLogoutSettings(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admSettings_EvntLogoutSettings Function in Window1.cs");
            //Adminsetting logout
            btnAdminLogin.Visibility = Visibility.Visible;
            GrdSettingsPanel.Children.Clear();
            LogHelper.Logger.Info("Ends admSettings_EvntLogoutSettings Function in Window1.cs");
        }
        void admLogin_EvntCloseAdminLogin(object sender, EventArgs e)
        {
            LogHelper.Logger.Info("Starts admLogin_EvntCloseAdminLogin Function in Window1.cs");
            //remove admin login screen from GrdSettingsPanel layout
            btnAdminLogin.Visibility = Visibility.Visible;
            AdminLogin admLogin = (AdminLogin)sender;
            GrdSettingsPanel.Children.Remove(admLogin);
            LogHelper.Logger.Info("Ends admLogin_EvntCloseAdminLogin Function in Window1.cs");
        }
        #endregion
        private void btnLogout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            UserLogout(sender);
        }

        private void UserLogout(object sender)
        {
            LogHelper.Logger.Info("Starts btnLogout_PreviewTouchUp Function in Window1.cs");
            Apps.FromAutoLogin = true;
            btnAdminLogin.Visibility = Visibility.Visible;
            btnBack_PreviewTouchUp_1(sender, null);
            brdUserCred.Visibility = Visibility.Collapsed;
            txtRoomName.Text = string.Empty;
            Apps.USERNAME = string.Empty;
            login = null;
            LogHelper.Logger.Info("Ends btnLogout_PreviewTouchUp Function in Window1.cs");
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            LogHelper.Logger.Info("Starts Window_Closing Function in Window1.cs");
            if (CardTerminalManager.Singleton.StartedUp)
            {
                CardTerminalManager.Singleton.Shutdown();
            }   
            idleTimer.Stop();
            Application.Current.Shutdown();
            Environment.Exit(0);
            LogHelper.Logger.Info("Ends Window_Closing Function in Window1.cs");
        }

        private void btnSearchRoom_TouchDown(object sender, TouchEventArgs e)
        {
            SearchRoom searchRoom = new SearchRoom();
            searchRoom.ShowDialog();
        }
    }
}